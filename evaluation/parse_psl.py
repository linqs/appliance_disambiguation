
import operator
import pickle
import json


def parse_row(row):
    '''
        returns [reading id,appliance set id/applliance id, truth value]
        '''
    time_and_app = (row).split(')')[0]
    
    
    app_or_combo_id = int(time_and_app.split(',')[1].strip(' ').strip('\''))
    time = int(time_and_app.split(',')[0].split('(')[1].strip('\''))
    
    truth_value = float((row).split('[')[1].split(']')[0])
    
    return [time,app_or_combo_id,truth_value]

def parse_results(results_folder,rfile):
    rows = []
    apps_on =  {}
    with open('../data/results/{}/{}'.format(results_folder,rfile),'r') as f:
        lines = f.readlines()
        for row in lines[1:-1]:
            try:
                parsed = parse_row(row)
                
                if parsed[1] not in apps_on:
                    apps_on[parsed[1]]={}
                apps_on[parsed[1]][parsed[0]]=parsed[2]
            except:
                print(row)
    time_dict={}
    
    for k,v in apps_on.items():
        
        for time,val in v.items():
            if time not in time_dict:
                time_dict[time]={}
            time_dict[time][k]=val



    return time_dict


def predict_app_set_from_time_dict(time_dict):
    to_return = {}

    
    for i in time_dict:
        this_dict = time_dict[i]
        sorted_this_dict =  sorted(this_dict.items(), key=operator.itemgetter(1),reverse=True)
        to_return[i]=sorted_this_dict[0][0]
    
    return to_return


def get_pickle_file(file_name):
    with open('{}.pkl'.format(file_name),'rb') as f:
        return pickle.load(f)


def get_json_file(file_name):
    with open('{}.json'.format(file_name),'r') as f:
        return json.load(f)




def dict_to_time_series(the_dict):
    start = min(the_dict.keys())
    end = max(the_dict.keys())
    return [the_dict[i] for i in range(start,end+1)]

def event_prediction_to_time_prediction(app_predictions,time_offset,start,end,data_dir):
    events = get_pickle_file('{}/event_lookup'.format(data_dir))
    
    to_return = {}
    for app,predictions in app_predictions.items():
        new_predictions={}
        
        for i in range(start,end):
            try:
                event = events['tte'][i]+time_offset
            
                prediction = predictions[event]
                new_predictions[i]=prediction
            except:
                pass

        to_return[app]=dict_to_time_series(new_predictions)



    return to_return


def get_app_values_from_app_set_predictions(result_directory,result_file_weights,temporal_type,data_dir,context,extension):
    
    if temporal_type=='interval':
        result_file = ("combo_infer_combo_{}_constraint_{}_consumption_{}_"
                       "toggle_{}_duration_{}_transition_{}.txt".format(result_file_weights['Combo'],result_file_weights['Constraint'],\
                                                                                    result_file_weights['Consumption'],result_file_weights['Toggle'],\
                                                                                    result_file_weights['Duration'],result_file_weights['Transition']))
    elif temporal_type=='instance':
        result_file = ("combo_infer_combo_{}_constraint_{}_consumption_{}_"
                       "toggle_{}_transition_{}.txt".format(result_file_weights['Combo'],result_file_weights['Constraint'],\
                                                                        result_file_weights['Consumption'],result_file_weights['Toggle'],\
                                                                        result_file_weights['Transition']))
    if int(context) and 'dataport' in result_directory:
        #result_file='{}{}'.format(result_file[:-4],'_context.txt')
        result_file='{}{}'.format(result_file[:-4],'{}.txt'.format(extension))

    predicted_app_set = predict_app_set_from_time_dict(parse_results(result_directory,result_file))
    
    app_set_dict = get_pickle_file('{}/combo_dict'.format(data_dir))

    offsets = get_pickle_file('{}/offset_dict'.format(data_dir))
    app_ids = get_pickle_file('{}/appliance_lookup'.format(data_dir))
    indices = get_pickle_file('{}/test_indices'.format(data_dir))
    
    app_set_offset = offsets['combo_offset']
    app_offset =  offsets['app_offset']


    all_apps={k+app_offset:{} for k in app_ids.keys()}
    return_keys = {v:k+app_offset for k,v in app_ids.items()}
    to_return = {v:[] for v in app_ids.values()}
    for event,predicted_combo in predicted_app_set.items():
        cl = app_set_dict['combo_lookup'][predicted_combo-app_set_offset]
        for k,v in to_return.items():
            on = int(k in cl)
            all_apps[return_keys[k]][event]=on

    if temporal_type=='interval':
        time_series_predictions  = event_prediction_to_time_prediction(all_apps,offsets['time_offset'],indices['start'],indices['stop'],data_dir)

        return {app_ids[k]:v for k,v in time_series_predictions.items()}
    else:
        return {app_ids[k]:dict_to_time_series(v) for k,v in all_apps.items()}


def get_app_values_from_app_set_predictions_instance(result_directory,result_file_weights,temporal_type,data_dir):
    
    if temporal_type=='interval':
        result_file = ("combo_infer_combo_{}_constraint_{}_consumption_{}_"
                       "toggle_{}_duration_{}_transition_{}.txt".format(result_file_weights['Combo'],result_file_weights['Constraint'],\
                                                                        result_file_weights['Consumption'],result_file_weights['Toggle'],\
                                                                        result_file_weights['Duration'],result_file_weights['Transition']))
    elif temporal_type=='instance':
        result_file = ("combo_infer_combo_{}_constraint_{}_consumption_{}_"
                       "toggle_{}_transition_{}.txt".format(result_file_weights['Combo'],result_file_weights['Constraint'],\
                                                            result_file_weights['Consumption'],result_file_weights['Toggle'],\
                                                            result_file_weights['Transition']))

    predicted_app_set = predict_app_set_from_time_dict(parse_results(result_directory,result_file))
    
    app_set_dict = get_pickle_file('{}/combo_dict'.format(data_dir))
    
    offsets = get_pickle_file('{}/offset_dict'.format(data_dir))
    app_ids = get_pickle_file('{}/appliance_lookup'.format(data_dir))
    indices = get_pickle_file('{}/test_indices'.format(data_dir))
    
    app_set_offset = offsets['combo_offset']
    app_offset =  offsets['app_offset']
    
    
    all_apps={k+app_offset:{} for k in app_ids.keys()}
    return_keys = {v:k+app_offset for k,v in app_ids.items()}
    to_return = {v:[] for v in app_ids.values()}
    for event,predicted_combo in predicted_app_set.items():
        cl = app_set_dict['combo_lookup'][predicted_combo-app_set_offset]
        for k,v in to_return.items():
            on = int(k in cl)
            all_apps[return_keys[k]][event]=on

    #time_series_predictions  = event_prediction_to_time_prediction(all_apps,offsets['time_offset'],indices['start'],indices['stop'],data_dir)
    
    return {app_ids[k]:dict_to_time_series(v) for k,v in all_apps.items()}



def get_true_app_states(data_dir):
    app_states = get_pickle_file('{}/on_dictionary'.format(data_dir))
    indices = get_pickle_file('{}/test_indices'.format(data_dir))

    to_return={k:v[indices['start']:indices['stop']] for k,v in app_states.items()}
    return to_return

def get_true_consumption(data_dir):
    uses = get_pickle_file('{}/use_lookup'.format(data_dir))
    return {k:[i for i in v] for k,v in uses.items()}


