import numpy as np
from sklearn.metrics import precision_recall_fscore_support as prfs
from sklearn.feature_extraction.text import TfidfTransformer
import pandas as pd
import scipy.stats
import pickle
import os
import operator
from sklearn.metrics import confusion_matrix
from collections import Counter
import pickle
from sklearn.metrics import accuracy_score


def round_to_binary(preds):
    return [int(x>.5) for x in preds]

def get_prfs(true,preds):
  
    if sum(true)==0:
        return ['na','na','na','na']
    else:
    
        return prfs(true,round_to_binary(preds),average='binary')

def calculate_f(p,r):
    return 2*(p*r)/(p+r)

def get_aggregate_results(true,preds):
    to_return = {}
    for k,v in true.items():
        
        to_return[k]=get_prfs(v,preds[k])
    return to_return


def app_mean(key,data_type,experiment_type,train_type,home_ids):
    all_apps ={}
    for home_id in home_ids:
        results = get_test_f(data_type,experiment_type,train_type,home_id)
        for k,v in results.items():
            if k not in all_apps:
                all_apps[k]=[]
            all_apps[k].append(v[key])
    return all_apps

def app_mean_from_result_files(key,result_files):
    all_apps ={}
    for home_id,results in result_files.items():
        for k,v in results.items():
            if k not in all_apps:
                all_apps[k]=[]
            if v[key]!='na':
                all_apps[k].append(v[key])
    return all_apps

def app_mean_from_result_files_months(key,result_files):
    all_apps ={}
    for home_id,results in result_files.items():
        for month in range(1,13):
            for k,v in results[month].items():
                if k not in all_apps:
                    all_apps[k]=[]
                if v[key]!='na':
                    all_apps[k].append(v[key])

    return all_apps


def calculate_f_all_homes(result_files):
    to_return = {}
    for home_id,app_results in result_files.items():
        temp = {}
        for app,results in app_results.items():
            temp[app]= get_prfs(results['true'],results['preds'])
        to_return[home_id]=temp
    return to_return

def f_from_r_and_p_from_result_files(result_files):
    to_return = {'Precision':{},'Recall':{},'F-Measure':{}}
    precision =  app_mean_from_result_files(0,result_files)
    recall = app_mean_from_result_files(1,result_files)
    for k,v in precision.items():
        to_return['Precision'][k]=np.array(v).mean()
        to_return['Recall'][k] = np.array(recall[k]).mean()
        to_return['F-Measure'][k]=calculate_f(to_return['Precision'][k],to_return['Recall'][k])

    to_return['Precision']['Average']=np.array(list(to_return['Precision'].values())).mean()
    to_return['Recall']['Average']=np.array(list(to_return['Recall'].values())).mean()
    to_return['F-Measure']['Average']=calculate_f(to_return['Precision']['Average'],to_return['Recall']['Average'])
    return to_return

def f_from_r_and_p_from_result_files_months(result_files):
    to_return = {'Precision':{},'Recall':{},'F-Measure':{}}
    precision =  app_mean_from_result_files_months(0,result_files)
    recall = app_mean_from_result_files_months(1,result_files)
    
    for k,v in precision.items():
        to_return['Precision'][k]=np.array(v).mean()
        to_return['Recall'][k] = np.array(recall[k]).mean()
        to_return['F-Measure'][k]=calculate_f(to_return['Precision'][k],to_return['Recall'][k])

    to_return['Precision']['Average']=np.array(to_return['Precision'].values()).mean()
    to_return['Recall']['Average']=np.array(to_return['Recall'].values()).mean()
    to_return['F-Measure']['Average']=calculate_f(to_return['Precision']['Average'],to_return['Recall']['Average'])

    return to_return


def f_from_r_and_p(data_type,experiment_type,train_type,home_ids):
    to_return = {}
    precision =  app_mean(0,data_type,experiment_type,train_type,home_ids)
    recall = app_mean(1,data_type,experiment_type,train_type,home_ids)
    for k,v in precision.items():
        to_return[k]=calculate_f(np.array(v).mean(),np.array(recall[k]).mean())
    return to_return



def get_mae(consumption,pred_consumption):
    return np.array([abs(consumption[i]-pred_consumption[i]) for i in range(len(pred_consumption))]).mean()
