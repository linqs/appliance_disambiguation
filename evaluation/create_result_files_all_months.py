import parse_psl
import evaluation_metrics as em
import os
import numpy as np
import json
import pickle
import sys
import pandas as pd


def get_home_ids(dataset):
    home_ids = {'dataport':[7951,3413,2859,6990,8292],'redd':[1,2,3,6]}
    return home_ids[dataset]


def save_a_home_f(result_dict,file_path):
    to_save = {}
    
    for k,v in result_dict.items():
        temp = {'Precision':v[0],'Recall':v[1],'F-Measure':v[2]}
        to_save[k]=temp
    pd.DataFrame(to_save).to_csv(file_path)

def save_a_home_mae(result_dict,file_path):
    to_save = {'MAE':result_dict}

    pd.DataFrame(to_save).to_csv(file_path)

def save_all_apps(all_apps,file_path):
  
    df=pd.DataFrame(all_apps).to_csv(file_path)

def get_weight_file_redd(data_type,test_type,home_id):
    with open('../data/initial_weights/{}/{}/home_{}_initial_weights.json'.format(data_type,test_type,home_id),'r') as f:
        return json.load(f)

def get_weight_file_dataport(data_type,test_type,home_id,month):
    with open('../data/initial_weights/{}/{}/home_{}_month_{}_initial_weights.json'.format(data_type,test_type,home_id,month),'r') as f:
        return {k:int(v) for k,v in json.load(f).items()}

def get_test_f_redd(data_type,experiment_type,train_type,home_id):
    results_directory = '{}/{}/{}/home_{}'.format(data_type,experiment_type,train_type,home_id)
    data_dir = '../data/to_evaluate/{}/{}/{}/home_{}'.format(data_type,experiment_type,train_type,home_id)
    result_file_weights = get_weight_file_redd(data_type,experiment_type,home_id)
    
    true = parse_psl.get_true_app_states(data_dir)
    

    preds = parse_psl.get_app_values_from_app_set_predictions(results_directory,result_file_weights,experiment_type,data_dir,0,'')

    results = em.get_aggregate_results(true,preds)
    return results

def get_test_f_dataport(data_type,experiment_type,train_type,temporal_resolution,context,home_id,month,extension):
    results_directory = '{}/{}/{}/{}/home_{}/month_{}'.format(data_type,experiment_type,train_type,temporal_resolution,home_id,month)
    #results_directory = '{}/{}/{}/new_context/home_{}/month_{}'.format(data_type,experiment_type,train_type,home_id,month)
    
    data_dir = '../data/to_evaluate/{}/{}/{}/{}/home_{}/month_{}'.format(data_type,experiment_type,train_type,temporal_resolution,home_id,month)
    result_file_weights = get_weight_file_dataport(data_type,experiment_type,home_id,month)
    true = parse_psl.get_true_app_states(data_dir)
   
    preds = parse_psl.get_app_values_from_app_set_predictions(results_directory,result_file_weights,experiment_type,data_dir,context,extension)

    
    
    return [true,preds]

def get_mae_dataport(data_type,experiment_type,train_type,temporal_resolution,context,home_id,month,extension,save_file_path_base):
    results_directory = '{}/{}/{}/{}/home_{}/month_{}'.format(data_type,experiment_type,train_type,temporal_resolution,home_id,month)
    #results_directory = '{}/{}/{}/new_context/home_{}/month_{}'.format(data_type,experiment_type,train_type,home_id,month)

    data_dir = '../data/to_evaluate/{}/{}/{}/{}/home_{}/month_{}'.format(data_type,experiment_type,train_type,temporal_resolution,home_id,month)
    result_file_weights = get_weight_file_dataport(data_type,experiment_type,home_id,month)
    true_consumption = parse_psl.get_true_consumption(data_dir)
    preds = parse_psl.get_app_values_from_app_set_predictions(results_directory,result_file_weights,experiment_type,data_dir,context,extension)
    stats = parse_psl.get_pickle_file('../data/consumption_stats/{}/{}/{}/home_{}/stats'.format(data_type,train_type,temporal_resolution,home_id))
    results = {}
    consumption_info ={}
    for k,v in preds.items():
        estimated_consumption=np.array(v)*stats[k]['mu']

        
        results[k] = em.get_mae(true_consumption[k],estimated_consumption)
        consumption_info[k] = [true_consumption[k],estimated_consumption]
    with open('{}/home_{}_month_{}_consumption_dict.pkl'.format(save_file_path_base,home_id,month),'wb') as f:
        pickle.dump(consumption_info,f)
    return results




def get_mae_redd(data_type,experiment_type,train_type,context,home_id,save_file_path_base):
    results_directory = '{}/{}/{}/home_{}'.format(data_type,experiment_type,train_type,home_id)
    data_dir = '../data/to_evaluate/{}/{}/{}/home_{}'.format(data_type,experiment_type,train_type,home_id)
    result_file_weights = get_weight_file_redd(data_type,experiment_type,home_id)
    true_consumption = parse_psl.get_true_consumption(data_dir)
    preds = parse_psl.get_app_values_from_app_set_predictions(results_directory,result_file_weights,experiment_type,data_dir,0,'')
    stats = parse_psl.get_pickle_file('../data/consumption_stats/{}/{}/home_{}/stats'.format(data_type,train_type,home_id))
    results = {}
    consumption_info ={}
    for k,v in preds.items():
        estimated_consumption=np.array(v)*stats[k]['mu']
    
        results[k] = em.get_mae(true_consumption[k],estimated_consumption)
    
        consumption_info[k] = [true_consumption[k],estimated_consumption]
    with open('{}/home_{}_consumption_dict.pkl'.format(save_file_path_base,home_id),'wb') as f:
        pickle.dump(consumption_info,f)

    return results


if __name__ == "__main__":
    dataset = sys.argv[1]
    experiment_type  =sys.argv[2]
    train =sys.argv[3]
    home_ids= get_home_ids(dataset)
    extension=''
    
    if dataset=='dataport':
        context =sys.argv[4]
        context_file=''
        temperature_file=''
        
        if int(context):
            extension ='_context'


        temporal_resolution =sys.argv[5]
        save_file_path_base = '../data/processed_results/{}/{}/{}/{}'.format(dataset,experiment_type,train,temporal_resolution)
        if not os.path.exists(save_file_path_base):
            os.makedirs(save_file_path_base)
        save_file_path_base =save_file_path_base+'/'
        result_files = {}
        for home_id in home_ids:
            temp = {}
            for month in range(1,13):
                print(month)
                result_dict = get_test_f_dataport(dataset,experiment_type,train,temporal_resolution,context,home_id,month,extension)
                for app, true in result_dict[0].items():
                    if app not in temp:
                        temp[app]={'true':[],'preds':[]}
                    temp[app]['true'].extend(true)
                    temp[app]['preds'].extend(result_dict[1][app])
            
            
                
               
                home_file_mae = '{}home_{}_month_{}_mae{}.csv'.format(save_file_path_base,home_id,month,extension)
                maes = get_mae_dataport(dataset,experiment_type,train,temporal_resolution,context,home_id,month,extension,save_file_path_base)
                save_a_home_mae(maes,home_file_mae)
        

            
            result_files[home_id]=temp
            with open('{}/predictions_truth_home_{}.pkl'.format(save_file_path_base,home_id),'wb') as f:
                pickle.dump(temp,f)
    
        home_fs = em.calculate_f_all_homes(result_files)
        all_apps_f = em.f_from_r_and_p_from_result_files(home_fs)
        save_path = '{}all_apps_precision_recall_fmeasure{}.csv'.format(save_file_path_base,extension)
        save_all_apps(all_apps_f,save_path)

    else:
        save_file_path_base = '../data/processed_results/{}/{}/{}'.format(dataset,experiment_type,train)
        if not os.path.exists(save_file_path_base):
            os.makedirs(save_file_path_base)
        save_file_path_base =save_file_path_base+'/'
        result_files = {}
        for home_id in home_ids:
            result_dict = get_test_f_redd(dataset,experiment_type,train,home_id)
            home_file = '{}home_{}_precision_recall_fmeasure.csv'.format(save_file_path_base,home_id)
            save_a_home_f(result_dict,home_file)
            result_files[home_id]=result_dict
            home_file_mae = '{}home_{}_mae_test_new.csv'.format(save_file_path_base,home_id)
            maes = get_mae_redd(dataset,experiment_type,train,0,home_id,save_file_path_base)
            save_a_home_mae(maes,home_file_mae)

        all_apps_f = em.f_from_r_and_p_from_result_files(result_files)
        save_path = '{}all_apps_precision_recall_fmeasure.csv'.format(save_file_path_base)
        save_all_apps(all_apps_f,save_path)

