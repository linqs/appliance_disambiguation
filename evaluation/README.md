To calculate the precision, recall, f-measure, and MAE for an experiment you can run python create_result_files_all_months.py. This requires several arguments. Here is an example processing the dataport results:
``` python create_result_files_all_months.py dataport interval test 1 hour```

Where dataport is the dataset, either dataport or redd, interval is the method, either interval or instance, test is the train/test condition, 1 indicates whether context was used, 1 if it was 0 if not, hour is the temporal resolution, either minute or hour.
The REDD dataset requires less parameters, here is a REDD example:
``` python create_result_files_all_months.py redd interval test```

This will create several files stored in data/processed_results/dataset/method/train_condition/temporal_resolution, e.g. data/processed_results/dataport/interval/test/hour All_apps_precision_recall_fmeasure.csv will contain the precision, recall, and f-measure across appliances. For each home, and in the case of dataport for each home and month, a file containing the mean aggregate error will be saved. This file will have mae in the title. 
