

This directory contains instructions for collecting Dataport and REDD data. 


* The data should be stored in data/original_data, Dataport should be stored in data/original_data/dataport_HDF5 and REDD should be stored in data/original_data/clean_redd
* Get the data. You will need permissions for both of the datasets used in the paper. To learn more: [REDD](http://redd.csail.mit.edu/), [Dataport](https://dataport.cloud/).
* The data processing scripts expect that the dataport homes will be saved as h5 files. To collect the data from Dataport and save each home in the appropriate format, refer to the ipython/juypter notebook get_from_postgres. Here we use the year 2015.  
    - You will need your username and password from Dataport for this step. 
* The data processing scripts expect that the REDD data is saved in a cvs format. It will already be saved in this format after downloading from the redd website. Howevever, we do one extra step and run preprocessing steps before using the data in the experiments. In particular we use Makonin's script, located [here](https://github.com/smakonin/DataWrangle_REDD).  

