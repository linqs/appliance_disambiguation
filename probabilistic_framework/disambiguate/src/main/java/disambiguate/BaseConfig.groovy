package disambiguate;

import org.linqs.psl.application.inference.MPEInference;
import org.linqs.psl.config.ConfigBundle;
import org.linqs.psl.config.ConfigManager;
import org.linqs.psl.database.Database;
import org.linqs.psl.database.DatabasePopulator;
import org.linqs.psl.database.DataStore;
import org.linqs.psl.database.Partition;
import org.linqs.psl.database.Queries;
import org.linqs.psl.database.ReadOnlyDatabase;
import org.linqs.psl.database.loading.Inserter;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver.Type;
import org.linqs.psl.database.rdbms.RDBMSDataStore;
import org.linqs.psl.groovy.PSLModel;
import org.linqs.psl.model.atom.Atom;
import org.linqs.psl.model.predicate.StandardPredicate;
import org.linqs.psl.model.term.ConstantType;
import org.linqs.psl.utils.dataloading.InserterUtils;
import org.linqs.psl.utils.evaluation.printing.AtomPrintStream;
import org.linqs.psl.utils.evaluation.printing.DefaultAtomPrintStream;
import org.linqs.psl.utils.evaluation.statistics.ContinuousPredictionComparator;
import org.linqs.psl.utils.evaluation.statistics.DiscretePredictionComparator;
import org.linqs.psl.utils.evaluation.statistics.DiscretePredictionStatistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.time.TimeCategory;
import java.nio.file.Paths;
import groovy.json.JsonSlurper;
/**
 * Sets up parameters for Interval and Instance. 
 *
 */

	/**
	 * Class for config variables
	 */
	public abstract class BaseConfig {
		public ConfigBundle cb;

		public String experimentName;
		public String dbPath;
		public String dataPath;
		public String outputPath;
        public String weightPath;
        public String weightExtensionFileName;

        public Map weightMap=[:];
        public List apps = [];
		public boolean sqPotentials = true;
        

		public boolean useTransitivityRule = false;
		public boolean useSymmetryRule = false;

		public BaseConfig(ConfigBundle cb) {
			this.cb = cb;
            //should be loop or one statement?
            //this.weightMap["Combo"] = weightMap["Combo"];
			//this.weightMap["Consumption"] = weightMap["Consumption"];
			//this.weightMap["Precedes"] = weightMap["Precedes"];
			//this.weightMap["Difference"] =weightMap["Difference"];
            //this.weightMap["Toggle"] =weightMap["Toggle"];
			
			//this.experimentName = cb.getString('experiment.name', 'default');
			//this.dbPath = cb.getString('experiment.dbpath', '/tmp');
			//this.dataPath = cb.getString('experiment.data.path', '../../data/psl_example/link_prediction');
			//this.outputPath = cb.getString('experiment.output.outputdir', Paths.get('output', this.experimentName).toString());
			this.useTransitivityRule = cb.getBoolean('model.rule.transitivity', false);
			this.useSymmetryRule = cb.getBoolean('model.rule.symmetry', false);
		}
        
        public void loadWeightsFromFile()
        {
        //read file
        def reader = new BufferedReader(new FileReader(this.weightPath));    
        //file to json
        def InputJSON = new JsonSlurper().parse(reader);
         
        //set weight map    
        InputJSON.each
        {
            entry -> this.weightMap[entry.key]=entry.value;    
        }
        }
        
        
    
        
        
                
        public void setWeights(Map weightMap)
        {
    
        this.weightMap = weightMap; 

        }
        
        public void loadList(String listName,String path)
        {
        //read file
            
        //../../data/input_for_psl/config_files/'
        def reader = new BufferedReader(new FileReader(path+listName+'.json'))    
        //file to json
        def InputJSON = new JsonSlurper().parse(reader);
         
        //set weight map
            
        if(listName=='appliances')
            {
            this.apps = InputJSON.appliances; 
            
            }
        else if (listName=='durations')
        {
            this.durations = InputJSON.durations;
           
        }
            
            
        else if (listName=='daytimes')
        {
            this.dayTimes = InputJSON.daytimes;
           
        }
        else if (listName=='days')
        {
            this.days = InputJSON.days;
           
        }
        else if (listName=='temperatures')
        {
            this.temperatures = InputJSON.temperatures;
           
        }
        }
	}
