package disambiguate;

import org.linqs.psl.application.inference.MPEInference;
import org.linqs.psl.config.ConfigBundle;
import org.linqs.psl.config.ConfigManager;
import org.linqs.psl.database.Database;
import org.linqs.psl.database.DatabasePopulator;
import org.linqs.psl.database.DataStore;
import org.linqs.psl.database.Partition;
import org.linqs.psl.database.Queries;
import org.linqs.psl.database.ReadOnlyDatabase;
import org.linqs.psl.database.loading.Inserter;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver.Type;
import org.linqs.psl.database.rdbms.RDBMSDataStore;
import org.linqs.psl.groovy.PSLModel;
import org.linqs.psl.model.atom.Atom;
import org.linqs.psl.model.predicate.StandardPredicate;
import org.linqs.psl.model.term.ConstantType;
import org.linqs.psl.utils.dataloading.InserterUtils;
import org.linqs.psl.utils.evaluation.printing.AtomPrintStream;
import org.linqs.psl.utils.evaluation.printing.DefaultAtomPrintStream;
import org.linqs.psl.utils.evaluation.statistics.ContinuousPredictionComparator;
import org.linqs.psl.utils.evaluation.statistics.DiscretePredictionComparator;
import org.linqs.psl.utils.evaluation.statistics.DiscretePredictionStatistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.time.TimeCategory;
import java.nio.file.Paths;

/**
 * Sets parameters for interval method. 
 */

	/**
	 * Class for config variables
	 */
	public class IntervalConfig extends BaseConfig{
        public List durations = []; 
        //add super?
		public IntervalConfig(ConfigBundle cb) {
            super(cb);
            this.weightPath =cb.getString('experiment.weightpath','');
            loadWeightsFromFile();
            
            this.dataPath = cb.getString('experiment.data.path','');
            
  
            
            loadList('appliances',dataPath);
          
            loadList('durations',dataPath);
            
         
            this.experimentName = cb.getString('experiment.name', 'interval');
            this.dbPath = cb.getString('experiment.dbpath', '/tmp');

            this.outputPath =cb.getString('experiment.output.outputdir','');
			
            this.weightExtensionFileName =         "combo_"+this.weightMap["Combo"]+"_constraint_"+this.weightMap["Constraint"]+"_consumption_"+this.weightMap["Consumption"]+"_toggle_"+this.weightMap["Toggle"]+"_duration_"+this.weightMap["Duration"]+"_transition_"+this.weightMap["Transition"];
           
            
		}
        
       
        //will add home id

        
	}
