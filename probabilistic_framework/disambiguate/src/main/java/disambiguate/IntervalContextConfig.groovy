package disambiguate;

import org.linqs.psl.application.inference.MPEInference;
import org.linqs.psl.config.ConfigBundle;
import org.linqs.psl.config.ConfigManager;
import org.linqs.psl.database.Database;
import org.linqs.psl.database.DatabasePopulator;
import org.linqs.psl.database.DataStore;
import org.linqs.psl.database.Partition;
import org.linqs.psl.database.Queries;
import org.linqs.psl.database.ReadOnlyDatabase;
import org.linqs.psl.database.loading.Inserter;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver.Type;
import org.linqs.psl.database.rdbms.RDBMSDataStore;
import org.linqs.psl.groovy.PSLModel;
import org.linqs.psl.model.atom.Atom;
import org.linqs.psl.model.predicate.StandardPredicate;
import org.linqs.psl.model.term.ConstantType;
import org.linqs.psl.utils.dataloading.InserterUtils;
import org.linqs.psl.utils.evaluation.printing.AtomPrintStream;
import org.linqs.psl.utils.evaluation.printing.DefaultAtomPrintStream;
import org.linqs.psl.utils.evaluation.statistics.ContinuousPredictionComparator;
import org.linqs.psl.utils.evaluation.statistics.DiscretePredictionComparator;
import org.linqs.psl.utils.evaluation.statistics.DiscretePredictionStatistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.time.TimeCategory;
import java.nio.file.Paths;

import groovy.json.JsonSlurper;

/**
 Loads all of the contextual parameters needed to run IntervalContext. 
 */

	/**
	 * Class for config variables
	 */
	public class IntervalContextConfig extends BaseConfig{
        public List dayTimes = [];
        public List days = [];
        public List temperatures = [];
        
        public Map daytimeOffWeights = [:];
        public Map daytimeOnWeights = [:];
        
        public Map dayOffWeights = [:];
        public Map dayOnWeights = [:];
        
        
        public Map temporalCombinedWeightsOn = [:];
        public Map temporalCombinedWeightsOff = [:];
        
        public Map temperatureOffWeights = [:];
        public Map temperatureOnWeights = [:];
        public List durations = []; 
        public String contextWeightPath;
        //add super?
		public IntervalContextConfig(ConfigBundle cb) {
            super(cb);
            
            this.weightPath =cb.getString('experiment.weightpath','');
            loadWeightsFromFile();
            
            this.dataPath = cb.getString('experiment.data.path','');
            loadList('appliances',dataPath);
            loadList('durations',dataPath);
            loadList('daytimes',dataPath);
            loadList('days',dataPath);
            loadList('temperatures',dataPath);
   
            this.contextWeightPath = cb.getString('experiment.data.contextWeightPath','');
            loadContextWeights();
           
            this.experimentName = cb.getString('experiment.name', 'interval');
            this.dbPath = cb.getString('experiment.dbpath', '/tmp');

            this.outputPath =cb.getString('experiment.output.outputdir','');

            this.weightExtensionFileName =         "combo_"+this.weightMap["Combo"]+"_constraint_"+this.weightMap["Constraint"]+"_consumption_"+this.weightMap["Consumption"]+"_toggle_"+this.weightMap["Toggle"]+"_duration_"+this.weightMap["Duration"]+"_transition_"+this.weightMap["Transition"]+'_context';
       
		}
        
        public void loadContextWeights()
        {
        //read file
        def reader = new BufferedReader(new FileReader(this.contextWeightPath+'/'+'daytimeOffWeights.json'));    
        //file to json
        def InputJSON = new JsonSlurper().parse(reader);
         
        //set weight map    
        InputJSON.each
        {
            entry -> this.daytimeOffWeights[entry.key]=entry.value;    
        }
            
        reader = new BufferedReader(new FileReader(this.contextWeightPath+'/'+'daytimeOnWeights.json'));    
        InputJSON = new JsonSlurper().parse(reader);    
        InputJSON.each
        {
            entry -> this.daytimeOnWeights[entry.key]=entry.value;    
        }    
          
        reader = new BufferedReader(new FileReader(this.contextWeightPath+'/'+'dayOffWeights.json'));    
        InputJSON = new JsonSlurper().parse(reader);    
        InputJSON.each
        {
            entry -> this.dayOffWeights[entry.key]=entry.value;    
        } 
            
        reader = new BufferedReader(new FileReader(this.contextWeightPath+'/'+'dayOnWeights.json'));    
        InputJSON = new JsonSlurper().parse(reader);    
        InputJSON.each
        {
            entry -> this.dayOnWeights[entry.key]=entry.value;    
        } 
            
        reader = new BufferedReader(new FileReader(this.contextWeightPath+'/'+'temperatureOffWeights.json'));    
        InputJSON = new JsonSlurper().parse(reader);    
        InputJSON.each
        {
            entry -> this.temperatureOffWeights[entry.key]=entry.value;    
        } 
            
        reader = new BufferedReader(new FileReader(this.contextWeightPath+'/'+'temperatureOnWeights.json'));    
        InputJSON = new JsonSlurper().parse(reader);    
        InputJSON.each
        {
            entry -> this.temperatureOnWeights[entry.key]=entry.value;    
        } 
            
            
        }
          public void loadContextWeightsCombined()
        {
        //read file
        def reader = new BufferedReader(new FileReader(this.contextWeightPath+'/'+'temporalCombinedOff.json'));    
        //file to json
        def InputJSON = new JsonSlurper().parse(reader);
         
        //set weight map    
        InputJSON.each
        {
            entry -> this.temporalCombinedWeightsOff[entry.key]=entry.value;    
        }
            
        reader = new BufferedReader(new FileReader(this.contextWeightPath+'/'+'temporalCombinedOn.json'));    
        InputJSON = new JsonSlurper().parse(reader);    
        InputJSON.each
        {
            entry -> this.temporalCombinedWeightsOn[entry.key]=entry.value;    
        }    
    
            
        reader = new BufferedReader(new FileReader(this.contextWeightPath+'/'+'temperatureOffWeights.json'));    
        InputJSON = new JsonSlurper().parse(reader);    
        InputJSON.each
        {
            entry -> this.temperatureOffWeights[entry.key]=entry.value;    
        } 
            
        reader = new BufferedReader(new FileReader(this.contextWeightPath+'/'+'temperatureOnWeights.json'));    
        InputJSON = new JsonSlurper().parse(reader);    
        InputJSON.each
        {
            entry -> this.temperatureOnWeights[entry.key]=entry.value;    
        } 
            
            
        }
    

        
	}
