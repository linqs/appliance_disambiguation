package disambiguate;

import org.linqs.psl.application.inference.MPEInference;
import org.linqs.psl.application.learning.weight.maxlikelihood.MaxLikelihoodMPE;
import org.linqs.psl.config.ConfigBundle;
import org.linqs.psl.config.ConfigManager;
import org.linqs.psl.database.Database;
import org.linqs.psl.database.DatabasePopulator;
import org.linqs.psl.database.DataStore;
import org.linqs.psl.database.Partition;
import org.linqs.psl.database.Queries;
import org.linqs.psl.database.ReadOnlyDatabase;
import org.linqs.psl.database.loading.Inserter;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver.Type;
import org.linqs.psl.database.rdbms.RDBMSDataStore;
import org.linqs.psl.groovy.PSLModel;
import org.linqs.psl.model.atom.Atom;
import org.linqs.psl.model.predicate.StandardPredicate;
import org.linqs.psl.model.term.ConstantType;
import org.linqs.psl.utils.dataloading.InserterUtils;
import org.linqs.psl.utils.evaluation.printing.AtomPrintStream;
import org.linqs.psl.utils.evaluation.printing.DefaultAtomPrintStream;
import org.linqs.psl.utils.evaluation.statistics.ContinuousPredictionComparator;
import org.linqs.psl.utils.evaluation.statistics.DiscretePredictionComparator;
import org.linqs.psl.utils.evaluation.statistics.DiscretePredictionStatistics;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.time.TimeCategory;
import java.nio.file.Paths;

/**
 * Infers the value of appliances being on and appliance sets being active using the instance method. 
 *

 */
public class Instance {
	private static final String PARTITION_TRAIN_OBSERVATIONS = "trainObservations";
	private static final String PARTITION_TRAIN_TARGETS = "trainTargets";
	private static final String PARTITION_TRAIN_TRUTH = "trainTruth";

    private static final String PARTITION_TEST_OBSERVATIONS = "testObservations";
    private static final String PARTITION_TEST_TARGETS = "testTargets";
	
    
	private Logger log;
	private DataStore ds;
	private InstanceConfig config;
	private PSLModel model;

    private static weightExtensionFileName;



	public Instance(ConfigBundle cb) {
		log = LoggerFactory.getLogger(this.class);
		config = new InstanceConfig(cb);
		ds = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, Paths.get(config.dbPath, 'disagg').toString(), true), cb);
		model = new PSLModel(this, ds);
	}

	/**
	 * Defines the logical predicates used in this model
	 */
	private void definePredicates() {
        
        //describe
		model.add predicate: "Combo", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        //describe
        //describe
		model.add predicate: "ActiveInCombo", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "IsOn", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "CloseToConsumption", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "Appliance", types: [ConstantType.UniqueID, ConstantType.Integer];
        //model.add predicate: "ComboID", types: [ConstantType.UniqueID, ConstantType.Integer];
        model.add predicate: "CandidateCombo", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "CandidateApp", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "Precedes", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "Difference", types: [ConstantType.UniqueID, ConstantType.UniqueID,ConstantType.UniqueID];
        model.add predicate: "Toggle", types: [ConstantType.UniqueID, ConstantType.UniqueID,ConstantType.UniqueID];
        model.add predicate: "CloseToDiff", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "Positive", types: [ConstantType.UniqueID];
        
        
	}

	/**
	 * Defines the rules for this model, optionally including transitivty and
	 * symmetry based on the PSLConfig options specified
	 */
	private void defineRules() {
		log.info("Defining model rules");
        
        model.add(
            rule: "Combo(T, +C) = 1",
			squared: config.sqPotentials,
			weight : config.weightMap["Constraint"]
		);
    
        
        //Consumption Rule
		model.add(
			rule: (CandidateCombo(T,C)&CloseToConsumption(T,C)) >> Combo(T,C),
			squared: config.sqPotentials,
			weight : config.weightMap["Consumption"]
		);
        
        //Toggle Rules
		model.add(
			rule: (CandidateCombo(Eone,Cone)&CandidateCombo(Etwo,Ctwo)&CandidateApp(Eone,X)&Difference(Eone,Etwo,Diff)&CloseToDiff(X,Diff)&~IsOn(Eone,X)&Positive(Diff)&Toggle(Cone,Ctwo,X)&Combo(Eone,Cone))>>Combo(Etwo,Ctwo),
			squared: config.sqPotentials,
			weight : config.weightMap["Toggle"]
		);

	
			model.add(
				rule: (CandidateCombo(Eone,Ctwo)&CandidateCombo(Etwo,Cone)&Difference(Eone,Etwo,Diff)&CandidateApp(Eone,X)&CloseToDiff(X,Diff)&IsOn(Eone,X)&~Positive(Diff)&Toggle(Cone,Ctwo,X)&Combo(Eone,Ctwo))>>Combo(Etwo,Cone),
				squared: config.sqPotentials,
				weight : config.weightMap["Toggle"]
			);

        //Combo and Appliance Propagation Rules

			model.add(
				rule: (CandidateApp(T,X)&CandidateCombo(T,C)&Combo(T,C)&ActiveInCombo(X,C)) >> IsOn(T,X),
				squared: config.sqPotentials,
				weight : config.weightMap["Combo"]
			);


		model.add(
			rule:(~IsOn(T,X) &CandidateCombo(T,C)& ActiveInCombo(X,C)) >> ~Combo(T,C),
			squared:config.sqPotentials,
			weight: config.weightMap["Combo"]
		);
        
        
        for (int i= 0; i< config.apps.size(); i++)
        {
            model.add(
			rule:(CandidateApp(Eone,Aone)&Precedes(Eone,Etwo)&Appliance(Aone,config.apps[i])&CandidateApp(Etwo,Aone)&IsOn(Eone,Aone)) >>                             IsOn(Etwo,Aone),
			squared:config.sqPotentials,
			weight: config.weightMap["Transition"]
		      );   
                        model.add(
			rule:(CandidateApp(Eone,Aone)&Precedes(Eone,Etwo)&Appliance(Aone,config.apps[i])&CandidateApp(Etwo,Aone)&~IsOn(Eone,Aone)) >>                             IsOn(Etwo,Aone),
			squared:config.sqPotentials,
			weight: config.weightMap["Transition"]
		      );
                        model.add(
			rule:(CandidateApp(Eone,Aone)&Precedes(Eone,Etwo)&Appliance(Aone,config.apps[i])&CandidateApp(Etwo,Aone)&IsOn(Eone,Aone)) >>                             ~IsOn(Etwo,Aone),
			squared:config.sqPotentials,
			weight: config.weightMap["Transition"]
		      );
                        model.add(
			rule:(CandidateApp(Eone,Aone)&Precedes(Eone,Etwo)&Appliance(Aone,config.apps[i])&CandidateApp(Etwo,Aone)&~IsOn(Eone,Aone)) >>                             ~IsOn(Etwo,Aone),
			squared:config.sqPotentials,
			weight: config.weightMap["Transition"]
		      );
        }
              

		log.debug("model: {}", model);
	}

	/**
	 * Load data from text files into the DataStore. Three partitions are defined
	 * and populated: observations, targets, and truth.
	 * Observations contains evidence that we treat as background knowledge and
	 * use to condition our inferences
	 * Targets contains the inference targets - the unknown variables we wish to infer
	 * Truth contains the true values of the inference variables and will be used
	 * to evaluate the model's performance
	 */
	private void loadTrainData(Partition obsPartition, Partition targetsPartition, Partition truthPartition) {
		log.info("Loading data into database");

		Inserter inserter = ds.getInserter(CandidateCombo, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Candidate_combos_train.txt").toString());
        
        inserter = ds.getInserter(CandidateApp, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Candidate_apps_train.txt").toString());

		inserter = ds.getInserter(CloseToConsumption, obsPartition);
		InserterUtils.loadDelimitedDataTruth(inserter, Paths.get(config.dataPath, "CloseToConsumption_train.txt").toString());

		inserter = ds.getInserter(Difference, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Differences_train.txt").toString());

        
        inserter = ds.getInserter(CloseToDiff, obsPartition);
		InserterUtils.loadDelimitedDataTruth(inserter, Paths.get(config.dataPath, "Distances.txt").toString());
        
        inserter = ds.getInserter(Positive, obsPartition);
		InserterUtils.loadDelimitedDataTruth(inserter, Paths.get(config.dataPath, "Positive.txt").toString());
        
        inserter = ds.getInserter(Toggle, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Toggle.txt").toString());
        
        inserter = ds.getInserter(Appliance, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Appliances.txt").toString());
        
        inserter = ds.getInserter(Precedes, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Precedes_train.txt").toString());
        
        inserter = ds.getInserter(ActiveInCombo, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "ActiveInCombo.txt").toString());
        
		inserter = ds.getInserter(Combo, targetsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Combo_train_target.txt").toString());
        
        inserter = ds.getInserter(IsOn, targetsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "IsOn_train_target.txt").toString());

		inserter = ds.getInserter(Combo, truthPartition);
		InserterUtils.loadDelimitedDataTruth(inserter, Paths.get(config.dataPath, "Combo_train.txt").toString());
        
        inserter = ds.getInserter(IsOn, truthPartition);
		InserterUtils.loadDelimitedDataTruth(inserter, Paths.get(config.dataPath, "IsOn_train.txt").toString());
	}

    	private void loadTestData(Partition obsPartition, Partition targetsPartition) {
		log.info("Loading data into database");

		Inserter inserter = ds.getInserter(CandidateCombo, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Candidate_combos_test.txt").toString());

        inserter = ds.getInserter(CandidateApp, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Candidate_apps_test.txt").toString());    
            
		inserter = ds.getInserter(CloseToConsumption, obsPartition);
		InserterUtils.loadDelimitedDataTruth(inserter, Paths.get(config.dataPath, "CloseToConsumption_test.txt").toString());

		inserter = ds.getInserter(Difference, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Differences_test.txt").toString());
        
        inserter = ds.getInserter(Appliance, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Appliances.txt").toString());
        
        inserter = ds.getInserter(Precedes, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Precedes_test.txt").toString());
        
        inserter = ds.getInserter(CloseToDiff, obsPartition);
		InserterUtils.loadDelimitedDataTruth(inserter, Paths.get(config.dataPath, "Distances.txt").toString());
        
        inserter = ds.getInserter(Positive, obsPartition);
		InserterUtils.loadDelimitedDataTruth(inserter, Paths.get(config.dataPath, "Positive.txt").toString());
        
        inserter = ds.getInserter(Toggle, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Toggle.txt").toString());
        
        inserter = ds.getInserter(ActiveInCombo, obsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "ActiveInCombo.txt").toString());
        
		inserter = ds.getInserter(Combo, targetsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "Combo_test_target.txt").toString());
        
        inserter = ds.getInserter(IsOn, targetsPartition);
		InserterUtils.loadDelimitedData(inserter, Paths.get(config.dataPath, "IsOn_test_target.txt").toString());

	
	}
    
    private void learnWeights(Partition obsPartition, Partition truthPartition, Partition targetPartition)
    {
       
        HashSet closed = new HashSet<StandardPredicate>([CandidateCombo,CandidateApp,CloseToConsumption,Difference,CloseToDiff,Positive,Toggle,ActiveInCombo,Appliance,Precedes]);
		
        Database observedDB = ds.getDatabase(targetPartition, closed, obsPartition);
        
        HashSet closedTwo = new HashSet<StandardPredicate>([IsOn,Combo]);
		
        
        Database trueDB = ds.getDatabase(truthPartition, closedTwo);
        
        MaxLikelihoodMPE weightLearning = new MaxLikelihoodMPE(model, observedDB, trueDB, config.cb);
        weightLearning.learn();
        weightLearning.close();
        observedDB.close();
        trueDB.close();
    }
    
	/**
	 * Run inference to infer the unknown Knows relationships between people.
	 */
	private void runInference(Partition obsPartition, Partition targetsPartition) {
		log.info("Starting inference");

		Date infStart = new Date();
		HashSet closed = new HashSet<StandardPredicate>([CandidateCombo,CandidateApp,CloseToConsumption,Difference,CloseToDiff,Positive,Toggle,ActiveInCombo,Appliance,Precedes]);
		Database inferDB = ds.getDatabase(targetsPartition, closed, obsPartition);
		MPEInference mpe = new MPEInference(model, inferDB, config.cb);
		mpe.mpeInference();
		mpe.close();
		inferDB.close();

		log.info("Finished inference in {}", TimeCategory.minus(new Date(), infStart));
	}

	/**
	 * Writes the output of the model into a file
	 */
	private void writeOutput(Partition targetsPartition) {
		Database resultsDB = ds.getDatabase(targetsPartition);
		PrintStream ps = new PrintStream(new File(Paths.get(config.outputPath, "combo_infer_"+config.weightExtensionFileName+".txt").toString()));
		AtomPrintStream aps = new DefaultAtomPrintStream(ps);
		Set atomSet = Queries.getAllAtoms(resultsDB,Combo);
		for (Atom a : atomSet) {
			aps.printAtom(a);
		}

		aps.close();
		ps.close();
        
        
        ps = new PrintStream(new File(Paths.get(config.outputPath, "app_infer_"+config.weightExtensionFileName+".txt").toString()));
        aps = new DefaultAtomPrintStream(ps);
        atomSet = Queries.getAllAtoms(resultsDB,IsOn);
		for (Atom a : atomSet) {
			aps.printAtom(a);
		}

		aps.close();
		ps.close();
        
        
		resultsDB.close();
	}

	

	public void run() {
		log.info("Running experiment {}", config.experimentName);

		Partition obsTrainPartition = ds.getPartition(PARTITION_TRAIN_OBSERVATIONS);
		Partition targetsTrainPartition = ds.getPartition(PARTITION_TRAIN_TARGETS);
		Partition truthTrainPartition = ds.getPartition(PARTITION_TRAIN_TRUTH);

        Partition obsTestPartition = ds.getPartition(PARTITION_TEST_OBSERVATIONS);
		Partition targetsTestPartition = ds.getPartition(PARTITION_TEST_TARGETS);
        
		definePredicates();
		defineRules();
		loadTrainData(obsTrainPartition, targetsTrainPartition, truthTrainPartition);
        loadTestData(obsTestPartition, targetsTestPartition);
        
        learnWeights(obsTrainPartition,truthTrainPartition,targetsTrainPartition);
		runInference(obsTestPartition, targetsTestPartition);
		writeOutput(targetsTestPartition);

		ds.close();
	}

	/**
	 * Parse the command line options and populate them into a ConfigBundle
	 * @param args - the command line arguments provided during the invocation
	 * @return - a ConfigBundle populated with options from the command line options
	 */
	public static ConfigBundle populateConfigBundle(String[] args) {
		ConfigBundle cb = ConfigManager.getManager().getBundle("disagg");
		if (args.length > 0) {
            String weightFile = ''; 
            def temporal = 'instance'
            String dataPath = '';
            String outputPath = '';
            if(args[0]=='redd'){
                 weightFile ='../../data/initial_weights/'+args[0]+'/' + temporal + '/home_'+ args[1]+'_initial_weights.json' ;
                 dataPath = '../../data/'+args[0]+'/'+temporal+'/'+args[2]+'/home_'+args[1]+'/';
                outputPath = '../../data/results/'+args[0]+'/'+temporal+'/'+args[2]+'/home_'+args[1]+'/';
            } 
            else{
                //add month
                dataPath = '../../data/'+args[0]+'/'+temporal+'/'+args[2]+'/'+args[4]+'/'+'/home_'+args[1]+'/'+'month_'+args[3]+'/';
                weightFile ='../../data/initial_weights/'+args[0]+'/' + temporal + '/home_'+ args[1]+'_month_'+args[3]+'_initial_weights.json' ;
                outputPath = '../../data/results/'+args[0]+'/'+temporal+'/'+args[2]+'/'+args[4]+'/'+'/home_'+args[1]+'/'+'month_'+args[3]+'/';
            }
            
            
			cb.setProperty('experiment.data.path', dataPath);
            cb.setProperty('experiment.weightpath', weightFile);
            cb.setProperty('experiment.output.outputdir',outputPath);
            
            
		}
		return cb;
	}

	/**
	 * Run this model from the command line
	 * @param args - the command line arguments
	 */
	public static void main(String[] args) {


        
        

		ConfigBundle configBundle = populateConfigBundle(args);
		Instance disagg = new Instance(configBundle);
		disagg.run();
        
     
	}
}
