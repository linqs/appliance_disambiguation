This is the project folder for disambiguate. All of the code files are in src/main/java/disambiguate. To run this code you will first need to install PSL. [PSL](http://psl.linqs.org/). After installing PSL, following [these instructions](https://github.com/linqs/psl/wiki/Developing-PSL), refer to the next instructions. For this code you will need PSL 2.0 which is in the develop branch of PSL.  

1. Compile your project
```
mvn compile
```
2. Now use Maven to generate a classpath for your project's dependencies:
```
mvn dependency:build-classpath -Dmdep.outputFile=classpath.out
```
3. To run execute one of the run scripts provided. For example: ./run_redd_interval.sh
4. Each of the run scripts uses the Xmx java flag to specify the amount of memory needed. You should change this flag to match your system specifications. 




