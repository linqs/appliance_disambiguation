from process_raw_data import process_dataset
import processing_duration
import pandas as pd
import pickle
import data_formatting as df
import numpy as np
import write_functions as wf
import operator
import json
import os

def to_duration_thresholds(durations):
    to_return = {}
    to_return[0]=[0,round(np.percentile(durations,25))]
    to_return[1]=[round(np.percentile(durations,25)),round(np.percentile(durations,50))]
    to_return[2]=[round(np.percentile(durations,50)),round(np.percentile(durations,75))]
    to_return[3]=[round(np.percentile(durations,75)),1e12]
    return to_return

def series_to_train(series_of_series,train_index):
    if len(series_of_series)==0:
        return []
    count = 0
    the_dict = {}
    good_series = []
    for series_index in range(len(series_of_series)):
        for val in series_of_series[series_index]:
            the_dict[val]=series_index
    
    return [series_of_series[the_dict[i]] for i in range(0,train_index) if i in the_dict]




def get_durations_apps(customs,home,target_apps,stop):
    to_return ={}
    ons = df.get_ons_home_modified(home,customs,target_apps)
    for app,states in ons.items():
        durations = processing_duration.get_durations(states[:stop],1)
        to_return[app]=durations
    return to_return

def fix_thresholds(thresholds):
    to_return = {}
    if thresholds[0][1]==1:
        offset = .1
        to_return = {}
        for k,v in thresholds.items():
            to_return[k]=[v[0]+offset,v[1]+offset]
        return to_return
    return thresholds

def get_ons(home,target_apps,min_on,start,stop):
    customs = process_dataset.get_custom_normal_training(home,target_apps,min_on,start,stop)
    ons = df.get_ons_home_modified(home,customs,target_apps)
    to_return = {a:[] for a in target_apps}
    for app,on in ons.items():
        durations = processing_duration.get_durations(on[start:stop],1)
        lens = [len(i) for i in durations]
        for l in lens:
            to_return[app].append(l)

    return to_return


def to_all_lens(apps):
    all_lens = []
    for app,lens in apps.items():
        all_lens.extend(lens)
    return all_lens



def durations_dataport():
    durations = {}
    target_apps = ['air1', 'furnace1', 'refrigerator1',  'clotheswasher1','drye1','dishwasher1', 'kitchenapp1', 'microwave1']
    
    for time_resolution in ['minute','hour']:
        all_durations_train = []
        all_durations_test = []
        for home_id in [7951,6990,2859,3413,8292]:
            
            home = process_dataset.get_home_dataport(home_id,'../data/original_data/dataport_HDF5',time_resolution)
            home = home*1000
            
            use_column='use'
            home = process_dataset.home_to_year(home,2015,1)
            
            stats_path ='{}stats.pkl'.format('../data/consumption_stats/dataport/{}/{}/home_{}/'.format('train',time_resolution,home_id))

            if os.path.isfile(stats_path):
    
                with open(stats_path,'rb') as f:
                    customs = pickle.load(f)
            

            months =  home.groupby(lambda x:x.month).groups
            
            for month_id,month_indices in months.items():
                month_df = home.loc[month_indices[0]:month_indices[-1]]
                
                train_index = int(len(month_df['use'])/2)
                
                
                temp_train = get_durations_apps(customs,month_df,target_apps,train_index)
                
                
                for app,v in temp_train.items():
                    
                    lens = [len(i) for i in v]
                    for l in lens:
                        all_durations_train.append(l)
    
        thresholds = to_duration_thresholds(all_durations_train)
        thresholds = fix_thresholds(thresholds)
        with open('../data/threshold_settings/duration_dict_dataport_{}.pkl'.format(time_resolution),'wb') as f:
            pickle.dump(thresholds,f)


    return durations





def durations_redd():
    durations = {}
    target_apps =  [' MICR',' DISH',' LITE',' REFG']
    
    
    
    
    all_durations_train = []
    all_durations_test = []
    for home_id in [1,2,3,6]:
        
        home = process_dataset.get_home_redd(home_id,'../data/original_data/clean_redd')
        use_column=' MAIN'
        
        if home_id==6:
            target_apps=[' LITE',' REFG']
    #customs = process_dataset.get_custom_normal_training(home,target_apps,20,0,int(len(home[use_column])/2))




        stats_path ='{}stats.pkl'.format('../data/consumption_stats/redd/{}/home_{}/'.format('train',home_id))
    
    
        if os.path.isfile(stats_path):
        
            with open(stats_path,'rb') as f:
                customs = pickle.load(f)
        
        train_index = int(len(home[use_column])/2)
     

        temp_train = get_durations_apps(customs,home,target_apps,train_index)

                
        for app,v in temp_train.items():
                    
            lens = [len(i) for i in v]
            for l in lens:
                all_durations_train.append(l)
            
        
    
    thresholds = to_duration_thresholds(all_durations_train)
    with open('../data/threshold_settings/duration_dict_redd.pkl','wb') as f:
        pickle.dump(thresholds,f)

if __name__=="__main__":
    durations_dataport()
    durations_redd()

