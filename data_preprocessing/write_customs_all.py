from process_raw_data import process_dataset
import processing_duration
import pandas as pd
import pickle
import data_formatting as df
import numpy as np
import write_functions as wf
import operator
import json
import os


def write_customs_home_dataport(home_id,temporal_resolution):
    train = 'train'
    target_apps = ['air1', 'furnace1', 'refrigerator1',  'clotheswasher1','drye1','dishwasher1', 'kitchenapp1', 'microwave1']
    home = process_dataset.get_home_dataport(home_id,'../data/original_data/dataport_HDF5',temporal_resolution)

    home = home*1000
    
    use_column='use'
    home = process_dataset.adjust_total_power_no_noise(home,target_apps,use_column)
    
    stats_path ='{}stats.pkl'.format('../data/consumption_stats/dataport/{}/{}/home_{}/'.format(train,temporal_resolution,home_id))
    
    if not os.path.isfile(stats_path):

    
        os.makedirs('../data/consumption_stats/dataport/{}/{}/home_{}/'.format(train,temporal_resolution,home_id))


    customs = process_dataset.get_custom_normal_training(home,target_apps,20,0,int(len(home[use_column])/2))
    
    
    
    with open(stats_path,'wb') as f:
        pickle.dump(customs,f)



def write_customs_home_redd(home_id):
    home = process_dataset.get_home_redd(home_id,'../data/original_data/clean_redd')


    target_apps =  [' MICR',' DISH',' LITE',' REFG']
    
    if int(home_id)==6:
        del target_apps[0]
        del target_apps[0]

    use_column = ' MAIN'
    train = 'train'
    
    home = process_dataset.adjust_total_power_no_noise(home,target_apps,use_column)
    
    stats_path ='{}stats.pkl'.format('../data/consumption_stats/redd/{}/home_{}/'.format(train,home_id))
    if not os.path.isfile(stats_path):
    
    
        os.makedirs('../data/consumption_stats/redd/{}/home_{}/'.format(train,home_id))


    customs = process_dataset.get_custom_normal_training(home,target_apps,20,0,int(len(home[use_column])/2))
        
    with open(stats_path,'wb') as f:
        pickle.dump(customs,f)


def customs_dataport(time_resolutions,home_ids):
    for time_resolution in time_resolutions:
        for home_id in home_ids:
            write_customs_home_dataport(home_id,time_resolution)

def customs_redd(home_ids):
    for home_id in home_ids:
        write_customs_home_redd(home_id)

if __name__=="__main__":
    customs_dataport(['hour','minute'],[8292,7951,3413,2859,6990])
    customs_redd([1,2,3,6])

