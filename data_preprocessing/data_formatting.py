import numpy as np
import pandas as pd
import datetime
import itertools
import scipy.stats as stats
import math

def dict_to_series(the_dict):
    first = min(the_dict.keys())
    last = max(the_dict.keys())
    return [[t,the_dict[t]] for t in range(first,last+1)]

def get_min_on_modified(app,new_custom):
    
    temp =  new_custom[app]['mu']-new_custom[app]['sigma']
    
    if temp>0:
        return temp
    return 20

def get_ons_home_modified(home,customs,app_ids):
    to_return={}
    for app in app_ids:
        if app!='TimeStamp ' and app!=' MAIN' and app!='use':
            to_return[app]=[int(x>get_min_on_modified(app,customs)) for x in home[app]]
    return to_return



def get_toggle(target_apps,combo_offset,app_offset,combos):
    
    toggle_ons= []
    toggle_ons= []
    to_return=[]
    for combo_one_index in range(len(combos)):
        for combo_two_index in range(combo_one_index,len(combos)):
            if combo_one_index==combo_two_index:
                continue
            
            
            combo_one = combos[combo_one_index]
            combo_two = combos[combo_two_index]
            for app_index in range(len(target_apps)):
                app=target_apps[app_index]
                
                missing_one =  list(set(combo_two)-(set(combo_one)))
                missing_two =  list(set(combo_one)-(set(combo_two)))
                symmetric = list(set(combo_one).symmetric_difference(combo_two))

                if len(missing_one)==1 and len(symmetric)==1 and missing_one[0]==app:
  
              
                    if (len(combo_one)+ len(combo_two))!=2:
                        to_return.append([combo_one_index+combo_offset,combo_two_index+combo_offset,app_index+app_offset])

                elif len(missing_two)==1 and len(symmetric)==1 and missing_two[0]==app:
                   
                          
                    if (len(combo_one)+ len(combo_two))!=2:
                        to_return.append([combo_two_index+combo_offset,combo_one_index+combo_offset,app_index+app_offset])


    return to_return

def assign_cluster(main,total_buckets):
    to_return = []
    offset = min(main)
    for t in main:
        distances = [abs(t-b) for b in total_buckets]
        cluster = distances.index(min(distances))
        to_return.append(cluster)
    return to_return


def special_round(num):
    if num>1:
        return 1
    elif num<0 or math.isnan(num):
        return 0
    else:
        return num

def simple_probability(point,energy_clusters,ghost_noise):
    distances = np.array([abs(point-energy_clusters[e_i]-ghost_noise)/max([point,energy_clusters[e_i]+ghost_noise]) if e_i!=0 else abs(point-energy_clusters[e_i]-ghost_noise)/max([point,ghost_noise]) for e_i in range(len(energy_clusters))])
    distances = [1-special_round(p) for p in distances]
    return distances

def get_actual_combos(use,customs,ons,new_customs,app_ids):

    buckets,combos,total_energy_clusters,cl = get_total_energy_clusters(use,new_customs,app_ids)
 
    combos_temp = []
    
    for i in range(len(ons[app_ids[0]])):
        temp = []
        for app in app_ids:
            if ons[app][i]:
                temp.append(app)
        combos_temp.append(temp)
    real_combos = []
    for c in combos_temp:
        for combo_i in range(len(cl)):
            if set(cl[combo_i])==set(c):
                real_combos.append(combo_i)

    return {'real_combos':real_combos,'buckets':buckets,'combos':combos,'combo_lookup':cl,'total_energy_clusters':total_energy_clusters}

def make_total_energy_clusters( stats,app_ids):

    combos = list(itertools.product([0, 1], repeat=len(stats)))
    
    total_buckets = []
    combo_lookup = []
    
    for c in combos:
        total=0
     
        tempt=[]
        for i in range(len(c)):
            if c[i]==1:
                tempt.append(app_ids[i])

            temp = c[i]*stats[i]['mu']
            
            total = total+temp

        if total<0:
            total=0
        total_buckets.append(total)
        combo_lookup.append(tempt)
    
    sorted_indices = sorted(range(len(total_buckets)), key=lambda k: total_buckets[k])
    new_buckets = sorted(total_buckets)
    new_combos = [combos[i] for i in sorted_indices]
    new_lookup = [combo_lookup[i] for i in sorted_indices]
    
    return new_buckets,new_combos,new_lookup


def to_write_active_in_combo_new(target_apps,combos,app_offset,combo_offset):
    to_return = []
    app_id_lookup = {target_apps[i]:i for i in range(len(target_apps))}
    #print(combos)
    for t in target_apps:
        for c in range(len(combos)):
            if t in combos[c]:
                
                to_return.append([app_id_lookup[t]+app_offset,c+combo_offset])
    return to_return


def get_total_energy_clusters(use,stats,app_ids):
    
    buckets,combos,cl = make_total_energy_clusters(stats,app_ids)
    cluster_assignments = assign_cluster(use,buckets)
    
    return buckets,combos,cluster_assignments,cl

def get_precedes(time_start,time_stop,time_offset):
    to_return = [[t+time_offset,t+1+time_offset] for t in range(time_start,time_stop)]
    del to_return[-1]
    return to_return

def instances_to_differences(total_power):
    to_return = []
    
    for time in range(len(total_power)-1):
        
        last_power = total_power[time]
        first_power = total_power[time+1]
        diff = int(first_power - last_power)
        to_return.append([time,time+1,diff])
    return to_return