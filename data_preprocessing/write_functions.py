import processing_duration

def dict_to_series(the_dict):
    first = min(the_dict.keys())
    last = max(the_dict.keys())
    return [[t,the_dict[t]] for t in range(first,last+1)]


def time_indexed_series_train_test_series(time_series,old_new_map,train_index,test_index):
    to_return = {}
    
    train_series = {}
    test_series = {}
    
    for time in range(train_index):

        train_series[old_new_map[time]]=time_series[time]

    to_return['train']= dict_to_series(train_series)

    for time in range(train_index,test_index):
       
        test_series[old_new_map[time]]=time_series[time]
    
    to_return['test']= dict_to_series(test_series)

    return to_return

def write_duration(event_dictionary,threshold_dict,train_index,validate_index,old_new_map):
    durations = to_write_duration_from_events(event_dictionary,threshold_dict)

    return time_indexed_series_train_test_series(durations,old_new_map,train_index,validate_index)


def write_a_psl_file(directory,filename, data):
    with open('{}{}.txt'.format(directory,filename),'w')  as f:
        for line in data:
            thisline = '\t'.join([str(i) for i in line])
            thisline = thisline+'\n'
            f.write(thisline)

