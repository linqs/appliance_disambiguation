from process_raw_data import process_dataset
import processing_duration
import pandas as pd
import pickle
import data_formatting as df
import numpy as np
import write_functions as wf
import operator
import write_context
import json

def ons_to_events(ons,event_start,event_stop,lookup,event_offset,app_offset,target_apps):
    
    to_return = []
    
    for event in range(event_start,event_stop):
        for app_id in range(len(target_apps)):
            all_ons = [ons[target_apps[app_id]][t] for t in lookup['ett'][event] ]
            value = round(np.array(all_ons).mean())
            
            to_return.append([event+event_offset,app_id+app_offset,value])
    return to_return

def ons_to_events_new(ons,event_start,event_stop,lookup,event_offset,app_offset,target_apps):
    
    to_return = []
    
    for event in range(event_start,event_stop):
        for app_id in range(len(target_apps)):
            times = lookup['ett'][event]
            time =  times[0]
            value = ons[target_apps[app_id]][time]
    
            
            to_return.append([event+event_offset,app_id+app_offset,value])
    return to_return

def combos_to_events(real_combos,event_start,event_stop,lookup,event_offset,combo_offset,combos):
    
    to_return = []
    
    for event in range(event_start,event_stop):
        for combo in combos:
            
            real_combo = round(np.array([real_combos[t] for t in lookup['ett'][event] ]).mean())
      
            to_return.append([event+event_offset,combo+combo_offset,float(combo==real_combo)])
    return to_return

def combos_to_events_new(real_combos,event_start,event_stop,lookup,event_offset,combo_offset,combos):
    
    to_return = []
    
    for event in range(event_start,event_stop):
        for combo in combos:
            times = lookup['ett'][event]
            time =  times[0]
            real_combo =real_combos[time]
       
            to_return.append([event+event_offset,combo+combo_offset,float(combo==real_combo)])
    return to_return


def write_ons(home,customs,target_apps,event_train_start,event_train_end,event_validate_start,event_validate_end,time_event_lookup,event_offset,app_offset,write_dir,to_evaluate_directory):
    
    ons = df.get_ons_home_modified(home,customs,target_apps)
    with open('{}on_dictionary.pkl'.format(to_evaluate_directory),'wb') as f:
        pickle.dump(ons,f)
    
    write_train = ons_to_events(ons,event_train_start,event_train_end,time_event_lookup,event_offset,app_offset,target_apps)
    
    wf.write_a_psl_file(write_dir,'IsOn_train',write_train)

    wf.write_a_psl_file(write_dir,'IsOn_train_target',[[t[0],t[1]] for t in write_train])
    
    write_test = ons_to_events(ons,event_validate_start,event_validate_end,time_event_lookup,event_offset,app_offset,target_apps)
    
    wf.write_a_psl_file(write_dir,'IsOn_test_target',[[t[0],t[1]] for t in write_test])


    return ons

def write_combos(customs,target_apps,readings,ons,event_dictionary,event_offset,combo_offset,event_train_start,event_train_end,event_validate_start,event_validate_end,write_dir,to_evaluate_directory):
    new_customs = {i:customs[target_apps[i]] for i in range(len(target_apps))}
    
    combos = df.get_total_energy_clusters(readings,new_customs,target_apps)
    
    combo_dict = df.get_actual_combos(readings,customs,ons,new_customs,target_apps)
    
    combo_train = combos_to_events(combo_dict['real_combos'],event_train_start,event_train_end,event_dictionary,event_offset,combo_offset,[i for i in range(2**len(target_apps))])
    
    combo_test = combos_to_events(combo_dict['real_combos'],event_validate_start,event_validate_end,event_dictionary,event_offset,combo_offset,[i for i in range(2**len(target_apps))])
    
    wf.write_a_psl_file(write_dir,'Combo_train',combo_train)
    wf.write_a_psl_file(write_dir,'Combo_train_target',[[t[0],t[1]] for t in combo_train])
    
    wf.write_a_psl_file(write_dir,'Combo_test_target',[[t[0],t[1]] for t in combo_test])
    
    
    with open('{}/combo_dict.pkl'.format(to_evaluate_directory),'wb') as f:
        pickle.dump(combo_dict,f)
    
    with open('{}/combo_dict_other.pkl'.format(to_evaluate_directory),'wb') as f:
        pickle.dump(combos,f)
    
    combo_ids = [c+combo_offset for c in range(2**len(target_apps))]

    with open('{}combos.json'.format(write_dir),'w') as f:
        json.dump({'combos':combo_ids},f)
    
    return combo_dict

def to_write_closeness_to_total_energy_events(readings,events,combo_dict,combo_offset,ghost_noise):
    
    to_return = {}
    
    for event,times in events.items():
        
        this_energy = np.array([readings[t] for t in times]).mean()
        
        probs = df.simple_probability(this_energy,combo_dict['buckets'],ghost_noise)
        
        temp  = []
        for p_i in range(len(probs)):
            temp.append([combo_offset+p_i,probs[p_i]])
        
        to_return[event]=temp
    return to_return


def times_probs_events(combo_probs_dict):
    to_return = {}
    for event,val in combo_probs_dict.items():
        time_index = event
        
        for entry in val:
            combo_id = entry[0]
            probability = entry[1]
            
            if time_index not in to_return:
                to_return[time_index]={}
            to_return[time_index][combo_id]=probability
    return to_return

def prune_keeping_probs(dict_form_probs,top_k,threshold):
    to_return = {}
    
    for k,v in dict_form_probs.items():
        sorted_probs = sorted(v.items(), key=operator.itemgetter(1),reverse=True)
        greater_than_threshold = [[k,vv] for k,vv in v.items() if vv>threshold]
        tops = [[ii for ii in i] for i in sorted_probs[:top_k]]
        
        if len(tops)>len(greater_than_threshold):
            to_return[k]=tops
        else:
            to_return[k]=greater_than_threshold
    return to_return


def prune_dict_to_file(pruned,event_start,event_stop,time_offset):
    to_return = []
    
    for event in range(event_start,event_stop):
        for reading in pruned[event]:
            to_return.append([event+time_offset,reading[0],reading[1]])
    return to_return

def write_close_to_consumption(readings,event_to_time,combo_dict,combo_offset,time_offset,ghost_noise,event_train_start,event_train_end,event_validate_start,event_validate_end,write_dir):
    
    
    closeness_lookup = to_write_closeness_to_total_energy_events(readings,event_to_time,\
                                                                 combo_dict,combo_offset,ghost_noise)
        
    dict_form = times_probs_events(closeness_lookup)
    pruned = prune_keeping_probs(dict_form,3,.5)
                                                                 
    write_train = prune_dict_to_file(pruned,event_train_start,event_train_end,time_offset)
    write_test = prune_dict_to_file(pruned,event_validate_start,event_validate_end,time_offset)
                                                                 
                                     
    wf.write_a_psl_file(write_dir,'CloseToConsumption_train',write_train)
                                                                 
    wf.write_a_psl_file(write_dir,'CloseToConsumption_test',write_test)
                                                                 
                                                                 
    return pruned


def pruned_to_apps(combo_offset,time_dict,combo_lookup,target_apps,app_offset,app_ids):
    
    to_return = {}
    
    for time,combos in time_dict.items():
        temp = []
        for c in combos:
            combo_index = c-combo_offset
            apps = combo_lookup[combo_index]
            for a in apps:
                app_id = app_ids[a]+app_offset
                if app_id not in temp:
                    temp.append(app_id)
        to_return[time]=temp
    return to_return

##Assumes Pruned
def write_candidates(pruned,combo_offset,time_offset,app_offset,combo_lookup,target_apps,event_train_start,event_train_end,event_validate_start,event_validate_end,write_dir):
    
    app_ids = {i:target_apps[i] for i in range(len(target_apps))}
    
    ##Write candidate apps
    apps_to_test=pruned_to_apps(combo_offset,{k:[vv[0] for vv in v] for k,v in pruned.items()},combo_lookup,target_apps,app_offset,{v:k for k,v in app_ids.items()})
    
    apps_to_infer_train = []
    #apps_to_infer_train_write = {}
    for k in range(event_train_start,event_train_end):
        temp = apps_to_test[k]
        #apps_to_infer_train_write[k]=apps_to_test[k]
        for vv in temp:
            apps_to_infer_train.append([k+time_offset,vv])

    apps_to_infer_validate = []
    apps_to_infer_validate_write = {}
    for k in range(event_validate_start,event_validate_end):
        temp = apps_to_test[k]
        apps_to_infer_validate_write[k+time_offset]=apps_to_test[k]
        for vv in temp:
            apps_to_infer_validate.append([k+time_offset,vv])

    wf.write_a_psl_file(write_dir,'Candidate_apps_train',apps_to_infer_train)
    wf.write_a_psl_file(write_dir,'Candidate_apps_test',apps_to_infer_validate)

    with open('{}apps_to_test.json'.format(write_dir),'w') as f:
        json.dump(apps_to_infer_validate_write,f)

    
    ##Write candidate combos
    combos_to_infer_train = []
    for k in range(event_train_start,event_train_end):
        for combo_val in pruned[k]:
            
            combos_to_infer_train.append([k+time_offset,combo_val[0]])

    combos_to_infer_validate = []
    combos_to_infer_write = {}
    for k in range(event_validate_start,event_validate_end):
        combos_to_infer_write[k+time_offset]=[]
        for combo_val in pruned[k]:
            combos_to_infer_write[k+time_offset].append(combo_val[0])
            combos_to_infer_validate.append([k+time_offset,combo_val[0]])

    wf.write_a_psl_file(write_dir,'Candidate_combos_train',combos_to_infer_train)
    wf.write_a_psl_file(write_dir,'Candidate_combos_test',combos_to_infer_validate)

    with open('{}combos_to_test.json'.format(write_dir),'w') as f:
        json.dump(combos_to_infer_write,f)

def get_duration_id(duration,threshold_dict):
    
    for k,v in threshold_dict.items():
        if duration>=v[0] and duration<v[1]:
            return k
    else:
        print(duration)

def to_write_duration_from_events(events,threshold_dict):
    ##Need to find new durations for this dataset, or find durations for different apps and then write a conversion function to handle different time scales
    
    to_return = {}
    
    for event,times in events.items():
        duration = get_duration_id(len(times),threshold_dict)
        to_return[event]=duration
    return to_return

def write_durations(events_to_times,duration_dict,event_train_start,event_train_end,event_validate_start,event_validate_end,time_offset,duration_offset,write_dir):
    all_durations = to_write_duration_from_events(events_to_times,duration_dict)
    
    to_write_train  = []
    
    for k in range(event_train_start,event_train_end):
        to_write_train.append([k+time_offset,all_durations[k]+duration_offset])
    
    to_write_validate  = []
    for k in range(event_validate_start,event_validate_end):
        to_write_validate.append([k+time_offset,all_durations[k]+duration_offset])
    
    wf.write_a_psl_file(write_dir,'Duration_train',to_write_train)
    wf.write_a_psl_file(write_dir,'Duration_test',to_write_validate)


def get_differences_from_events(event_start,event_stop,events,readings):
    
    to_return = []
    
    for event in range(event_start,event_stop-1):
        
        last_time = max(events[event])
        first_time = min(events[event+1])
        
        last_power = readings[last_time]
        first_power = readings[first_time]
        
        diff = int(first_power - last_power)
        to_return.append([event,event+1,diff])
    
    return to_return

def get_distance_to_difference(mean,difference):
    difference = abs(difference)
    distance = abs(mean-difference)
    normalized = distance/max([mean,difference])
    if normalized>1:
        return 1
    elif normalized<0:
        return 0
    else:
        return 1-normalized

def get_all_distances(differences,customs,new_app_ids):
    to_return = []
    
    for app,stats in customs.items():
        mean = stats['mu']
        for difference in differences:
            distance = get_distance_to_difference(mean,difference[2])
            app_id = new_app_ids[app]
            to_return.append([app_id,difference[2],distance])
    return to_return

def merge_distances(train_distances,validate_distances,diff_min):
    seen = {}
    to_return =[]
    for distance in train_distances:
        key = distance[1]+diff_min
        value = distance[2]
        if key not in seen:
            seen[key]=[]
        if value not in seen[key]:
            to_return.append(distance)
        seen[key].append(value)
    
    for distance in validate_distances:
        key = distance[1]+diff_min
        value = distance[2]
        if key not in seen:
            seen[key]=[]
        if value not in seen[key]:
            to_return.append(distance)
        seen[key].append(value)
    
    return to_return

def merge_positives(train_positives,validate_positives,diff_min):
    seen = {}
    to_return =[]
    for distance in train_positives:
        key = int(distance[0]+diff_min)
        value = int(distance[1])
        if key not in seen:
            seen[key]=[]
        if value not in seen[key]:
            to_return.append(distance)
        seen[key].append(value)
    
    for distance in validate_positives:
        key = int(distance[0]+diff_min)
        value = int(distance[1])
        if key not in seen:
            seen[key]=[]
        if value not in seen[key]:
            to_return.append(distance)
        seen[key].append(value)
    
    return to_return

def all_positive_differences(differences,diff_min):
    return [[i[2]+diff_min,int(i[2]>0) ] for i in differences ]

def write_differences(event_train_start,event_train_end,event_validate_start,event_validate_end,events_to_times,readings,customs,target_apps,diff_min,app_offset,diff_offset,time_offset,write_dir):
    
    train_differences = get_differences_from_events(event_train_start,event_train_end,events_to_times,readings)
    validate_differences = get_differences_from_events(event_validate_start,event_validate_end,events_to_times,readings)
    
    new_app_ids = {target_apps[i]:i for i in range(len(target_apps))}
    
    train_distances = get_all_distances(train_differences,customs,new_app_ids)
    validate_distances = get_all_distances(validate_differences,customs,new_app_ids)
    
    
    all_distances = merge_distances(train_distances,validate_distances,diff_min)
    
    is_positive = merge_positives(all_positive_differences(train_differences,diff_min),all_positive_differences(validate_differences,diff_min),diff_min)
    
    to_write_distances =[[i[0]+app_offset,i[1]+diff_min,i[2]] for i in all_distances]
    to_write_differences_train = [[i[0]+time_offset,i[1]+time_offset,i[2]+diff_offset+diff_min] for i in train_differences]
    to_write_differences_validate = [[i[0]+time_offset,i[1]+time_offset,i[2]+diff_offset+diff_min] for i in validate_differences]
    to_write_positive = is_positive
    
    wf.write_a_psl_file(write_dir,'Differences_train',to_write_differences_train)
    wf.write_a_psl_file(write_dir,'Differences_test',to_write_differences_validate)
    wf.write_a_psl_file(write_dir,'Distances',to_write_distances)
    wf.write_a_psl_file(write_dir,'Positive',to_write_positive)


def get_toggle(target_apps,combo_offset,app_offset,combos):
    
    toggle_ons= []
    toggle_ons= []
    to_return=[]
    for combo_one_index in range(len(combos)):
        for combo_two_index in range(len(combos)):
            combo_one = combos[combo_one_index]
            combo_two = combos[combo_two_index]
            for app_index in range(len(target_apps)):
                app=target_apps[app_index]
                
                missing =  list(set(combo_two)-(set(combo_one)))
                symmetric = list(set(combo_one).symmetric_difference(combo_two))
                if len(missing)==1 and len(symmetric)==1 and missing[0]==app:
                    #to_return.append([combo_one_index+combo_offset,combo_two_index+combo_offset,app_index+app_offset])
                    #to_return.append([combo_two_index+combo_offset,combo_one_index+combo_offset,app_index+app_offset])
                    if len(combo_one)+ len(combo_two)!=2:
                        to_return.append([combo_one_index+combo_offset,combo_two_index+combo_offset,app_index+app_offset])
    #to_return.append([combo_two_index,combo_one_index+combo_offset,app_index+app_offset])

    return to_return

def write_use(home,target_apps,start,stop,to_evaluate_directory):
    to_return = {}
    for app in target_apps:
        to_return[app]=home[app][start:stop]
    
    with open('{}use_lookup.pkl'.format(to_evaluate_directory),'wb') as f:
                pickle.dump(to_return,f)

def write_toggles(target_apps,combo_offset,app_offset,write_dir,combos):
    toggles = df.get_toggle(target_apps,combo_offset,app_offset,combos)
    wf.write_a_psl_file(write_dir,'Toggle',toggles)

def write_precedes(event_train_start,event_train_end,event_validate_start,event_validate_end,time_offset,write_dir):
    precedes_train = df.get_precedes(event_train_start,event_train_end,time_offset)
    precedes_validate = df.get_precedes(event_validate_start,event_validate_end,time_offset)
    
    wf.write_a_psl_file(write_dir,'Precedes_train',precedes_train)
    wf.write_a_psl_file(write_dir,'Precedes_test',precedes_validate)

def write_appliance(app_offset,target_apps,write_dir):
    wf.write_a_psl_file(write_dir,'Appliances',[[a+app_offset,a+app_offset] for a in range(len(target_apps))])

def write_combo(combo_offset,target_apps,write_dir):
    wf.write_a_psl_file(write_dir,'Combos',[[c+combo_offset,c+combo_offset] for c in range(2**len(target_apps))])

def write_aic(target_apps,combo_lookup,app_offset,combo_offset,write_dir):
    aic = df.to_write_active_in_combo_new(target_apps,combo_lookup,app_offset,combo_offset)
    wf.write_a_psl_file(write_dir,'ActiveInCombo',aic)

def get_experiment_settings(experiment_directory,filename):
    with open('{}/{}_dict.pkl'.format(experiment_directory,filename),'rb') as f:
        return pickle.load(f)

def write_config_files(write_dir,offsets,num_durations,num_apps):
    apps = [i+offsets['app_offset'] for i in range(num_apps)]
    durations = [i+offsets['duration_offset'] for i in range(num_durations)]

    write_context.write_json(write_dir,'durations',{'durations':durations})
    write_context.write_json(write_dir,'appliances',{'appliances':apps})


def write_interval(offsets,home,readings,event_threshold,use_column,customs,train_condition,ghost_noise,target_apps,duration_dict,write_dir,context,num_durations,to_evaluate_directory):
    ##initial things are needed like offsets
    ##events event map
    
    
    if train_condition=='validate':
        train_index =  int(len(home[use_column])/2)
        validate_index = int(len(home[use_column])/2 + len(home[use_column])/4)
    else:
        train_index = int(len(home[use_column])/2 + len(home[use_column])/4)
        validate_index = len(home[use_column])
    
    home_index = home.index
    
    event_dictionary = processing_duration.get_events_with_time_index(readings,event_threshold,home_index)
    
    new_event_dictionary = processing_duration.get_new_event_lookup(event_dictionary,train_index,validate_index,home_index)

    with open('{}event_lookup.pkl'.format(to_evaluate_directory),'wb') as f:
        pickle.dump(new_event_dictionary,f)

    with open('{}test_indices.pkl'.format(to_evaluate_directory),'wb') as f:
        pickle.dump({'start':train_index,'stop':validate_index},f)
    
    with open('{}indices_to_infer.json'.format(write_dir),'w') as f:
        json.dump({'start_train':offsets['time_offset'],'stop_train':new_event_dictionary['event_train']+offsets['time_offset'],'start_test':new_event_dictionary['event_train']+offsets['time_offset'],'stop_test':new_event_dictionary['event_validate']+offsets['time_offset']},f)

    if context:
        write_context.write_context_interval(home,home_index,offsets,new_event_dictionary,write_dir,24,7,4)

    write_config_files(write_dir,offsets,num_durations,len(target_apps))
    write_use(home,target_apps,train_index,validate_index,to_evaluate_directory)

    ##Write Ons
    ons  = write_ons(home,customs,target_apps,0,new_event_dictionary['event_train'],new_event_dictionary['event_train'],new_event_dictionary['event_validate'],new_event_dictionary,offsets['time_offset'],offsets['app_offset'],write_dir,to_evaluate_directory)


    
    ##Write Combos
    combo_dict = write_combos(customs,target_apps,home[use_column],ons,new_event_dictionary,offsets['time_offset'],offsets['combo_offset'],0,new_event_dictionary['event_train'],new_event_dictionary['event_train'],new_event_dictionary['event_validate'],write_dir,to_evaluate_directory)
    
    ##Close to Consumption
    pruned = write_close_to_consumption(home[use_column],new_event_dictionary['ett'],combo_dict,offsets['combo_offset'],offsets['time_offset'],ghost_noise,0,new_event_dictionary['event_train'],new_event_dictionary['event_train'],new_event_dictionary['event_validate'],write_dir)
    
    ##Write Candidates
    candidates = write_candidates(pruned,offsets['combo_offset'],offsets['time_offset'],\
                                  offsets['app_offset'],combo_dict['combo_lookup'],target_apps,\
                                  0,new_event_dictionary['event_train'],new_event_dictionary['event_train'],new_event_dictionary['event_validate'],\
                                  write_dir)
        
                                  ##Write Duration
    write_durations(new_event_dictionary['ett'],duration_dict,\
                                                  0,new_event_dictionary['event_train'],new_event_dictionary['event_train'],new_event_dictionary['event_validate'],\
                                                  offsets['time_offset'],offsets['duration_offset'],write_dir)
                                  
                                  
                                  
                                  ##Write Differences
                                  
    write_differences( 0,new_event_dictionary['event_train'],new_event_dictionary['event_train'],new_event_dictionary['event_validate'],\
                                                    new_event_dictionary['ett'],home[use_column],customs,target_apps,int(offsets['max_difference']/2),\
                                                    offsets['app_offset'],offsets['difference_offset'],offsets['time_offset'],write_dir)
                                  
                                  ##Write Toggle
    write_toggles(target_apps,offsets['combo_offset'],offsets['app_offset'],write_dir,combo_dict['combo_lookup'])
                                  
                                  ##Write Precedes 
    write_precedes(0,new_event_dictionary['event_train'],new_event_dictionary['event_train'],new_event_dictionary['event_validate'],offsets['time_offset'],write_dir)
                                  
                                  
                                  ##Write Appliance
    write_appliance(offsets['app_offset'],target_apps,write_dir)
                                  
                                  ##Write Combo
    write_combo(offsets['combo_offset'],target_apps,write_dir)
                                  
                                  ##Write ActiveInCombo
    write_aic(target_apps,combo_dict['combo_lookup'],\
                                            offsets['app_offset'],offsets['combo_offset'],write_dir)





##context written elsewhere

