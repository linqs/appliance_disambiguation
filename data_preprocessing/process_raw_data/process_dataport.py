import process_raw_data  as prd


def get_a_home(homeid,directory,dataset,adjust_type,target_apps):
    
    use_columns = {'dataport':'use','redd':' MAIN'}
    time_columns = {'dataport':'','redd':'TimeStamp '}
    
    if dataset=='dataport':
        home = prd.get_home_dataport(homeid,directory)
    else:
        home = prd.get_a_home_redd(homeid,directory)

    if adjust_type=='sum_of_target':
        home = prd.adjust_total_power_no_noise(home,target_apps,use_columns[dataset])
    elif adjust_type=='observed_minus_not_target':
        home = prd.adjust_total_power_no_noise(home,target_apps,use_columns[dataset],time_columns[dataset])

    return home

