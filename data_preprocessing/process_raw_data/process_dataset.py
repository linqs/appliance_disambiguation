from pandas import HDFStore,DataFrame
import pandas as pd
import numpy as np
import json

##retains more noise
def adjust_total_power(home,target_apps,use_column,time_column):
    #'TimeStamp ' for REDD
    subtract_columns = [col for col in home.columns if col not in target_apps and col!=time_column and col!=use_column and col!=' DIFF']
    home[use_column] = home[use_column]-home[subtract_columns].sum(axis=1)
    return home

def adjust_total_power_no_noise(home,target_apps,use_column):
    home[use_column] = home[target_apps].sum(axis=1)
    return home

##assumes dataport homes are stored as hdfStore's from dataport
def get_home_dataport(homeid,directory,time_resolution):
    ##should move these files eventually
    #../../fall_2016_disaggregating/april_psl/sum/data/dataport/homes
    store = HDFStore('{}/dataid_{}_{}.h5'.format(directory,homeid,time_resolution))
    if 'df' in store:
        df=store['df']
        df=df.sort_index()
        store.close()
    else:
        return 'error'
    return df

##assumes redd home is a csv
def get_home_redd(homeid,directory):
    return pd.read_csv('{}/REDDhouse{}_lowf_VA.csv'.format(directory,homeid))


def get_a_home(homeid,directory,dataset,adjust_type,target_apps):
    
    use_columns = {'dataport':'use','redd':' MAIN'}
    time_columns = {'dataport':'','redd':'TimeStamp '}
    
    if dataset=='dataport':
        home = get_home_dataport(homeid,directory)
    else:
        home = get_a_home_redd(homeid,directory)
    
    if adjust_type=='sum_of_target':
        home = adjust_total_power_no_noise(home,target_apps,use_columns[dataset])
    elif adjust_type=='observed_minus_not_target':
        home = adjust_total_power_no_noise(home,target_apps,use_columns[dataset],time_columns[dataset])
    
    return home

def get_custom_normal_training(home,apps,min_on,start,stop):
    new_stats={}
    
    for app in apps:
        app_series =np.array([k for k in home[app][start:stop] if k>min_on])
        new_stats[app]={'mu':app_series.mean(),'sigma':app_series.std()}
    
    return new_stats

def home_to_year(home,year,threshold):
    
    
    year_groups  = home.groupby(lambda x:x.year).groups
    if year not in year_groups:
        return False
    one = year_groups[year][0]
    two = year_groups[year][-1]
    
    year_df = home.loc[one:two]
    return year_df

