from process_raw_data import process_dataset
import processing_duration
import pandas as pd
import pickle
import data_formatting as df
import numpy as np
import write_functions as wf
import operator
import json


def get_hours(home):
    return [t.hour for t in home]

def get_days(home):
    return [t.dayofweek for t in home]

def average_hours_rounded(events_to_time_index):
    to_return = {}
    
    for event,times in events_to_time_index.items():
        
        hour_array = [i.hour for i in times]
        if 23 in hour_array and 0 in hour_array:
            if hour_array.count(0)>hour_array.count(23):
                    
                to_return[event]=0
            else:
                to_return[event]=23
        else:
            to_return[event]= int(np.array(hour_array).mean())

    return to_return

def event_days_rounded(event_to_time_indices):
    to_return = {}
    
    for event,times in event_to_time_indices.items():
        
        day_array = [i.dayofweek for i in times]
        if 6 in day_array and 0 in day_array:
            if day_array.count(0)>day_array.count(6):
                
                to_return[event]=0
            else:
                to_return[event]=6
        else:
            to_return[event]= int(np.array(day_array).mean())
    return to_return

def write_hours_interval(home,event_to_time_index,time_offset,hour_offset,train_start,train_end,validate_start,validate_end,write_dir):
    
    average_hours = average_hours_rounded(event_to_time_index)
    
    
    train=[[e+time_offset,average_hours[e]+hour_offset] for e in range(train_start,train_end)]
    validate=[[e+time_offset,average_hours[e]+hour_offset] for e in range(validate_start,validate_end)]
    wf.write_a_psl_file(write_dir,'TimeOfDay_train',train)
    wf.write_a_psl_file(write_dir,'TimeOfDay_test',validate)

def write_hours_instance(home,time_offset,hour_offset,train_start,train_end,validate_start,validate_end,write_dir):
    
    hours = get_hours(home)
    train=[[e+time_offset,hours[e]+hour_offset] for e in range(train_start,train_end)]
    validate=[[e+time_offset,hours[e]+hour_offset] for e in range(validate_start,validate_end)]
    wf.write_a_psl_file(write_dir,'Hours_train',train)
    wf.write_a_psl_file(write_dir,'Hours_test',validate)

def write_days_interval(home,event_to_time_index,time_offset,day_offset,train_start,train_end,validate_start,validate_end,write_dir):
    days = event_days_rounded(event_to_time_index)
    
    wf.write_a_psl_file(write_dir,'Day_train',[[e+time_offset,days[e]+day_offset] for e in range(train_start,train_end)])
    wf.write_a_psl_file(write_dir,'Day_test',[[e+time_offset,days[e]+day_offset] for e in range(validate_start,validate_end)])

def write_days_instance(home,time_offset,day_offset,train_start,train_end,validate_start,validate_end,write_dir):
    days = get_days(home)
    
    wf.write_a_psl_file(write_dir,'Day_train',[[e+time_offset,days[e]+day_offset] for e in range(train_start,train_end)])
    wf.write_a_psl_file(write_dir,'Day_test',[[e+time_offset,days[e]+day_offset] for e in range(validate_start,validate_end)])


def timestamp_to_hour(x):
    return x.replace(minute=0)


def get_average_temperature(all_temps):
    return int(round(np.array(all_temps).mean()))

def get_temperature(temperature_path,year):
    
    with open('{}austin_temperature_df_and_thresholds_{}.pkl'.format(temperature_path,year),'rb') as f:
        return pickle.load(f)

def write_temperature_instance(home_index,time_offset,temperature_offset,train_start,train_end,validate_start,validate_end,write_dir,temp_path):
    temperatures = get_temperature(temp_path,2015)['temp_df']
    to_write_train = []
    for i in range(train_start,train_end):
        
        last_good_temperature = -1
        
        
        ind = home_index[i]
        ind = timestamp_to_hour(ind)
        
        if ind not in temperatures.index:
            temperature = last_good_temperature
    
        else:
            temperature = temperatures.ix[ind]['temperature']
            if type(temperature)!=np.int64 and len(temperature)>1:
                temperature=temperature[0]
                last_good_temperature = temperature
        

        to_write_train.append([i+time_offset,temperature+temperature_offset])
                        
    wf.write_a_psl_file(write_dir,'Temperature_train',to_write_train)
                            
    to_write_validate = []
    for i in range(validate_start,validate_end):
                                    
        last_good_temperature =-1
                                        
        ind = home_index[i]
        ind = timestamp_to_hour(ind)
                                                
        if ind not in temperatures.index:
            
            temperature = last_good_temperature

        else:
                                                            
            temperature = temperatures.ix[ind]['temperature']
            if type(temperature)!=np.int64 and len(temperature)>1:
                temperature=temperature[0]
                last_good_temperature = temperature
                                                                            
        to_write_validate.append([i+time_offset,temperature+temperature_offset])
                                                                                
    wf.write_a_psl_file(write_dir,'Temperature_test',to_write_validate)


def write_temperature_interval(home_index,time_offset,temperature_offset,event_to_time,event_train_start,event_train_end,event_validate_start,event_validate_end,write_dir,temp_path):
    temperatures = get_temperature(temp_path,2015)['temp_df']
    to_write_train = []
    for i in range(event_train_start,event_train_end):
        temp_temps = []
        last_good_temperature = -1
        for time in event_to_time[i]:
            
            ind = home_index[time]
            ind = timestamp_to_hour(ind)
            
            if ind not in temperatures.index:
                #print(ind)
                #print(last_good_temperature)
                temperature = last_good_temperature
            
            #print(index)
            else:
                
                temperature = temperatures.ix[ind]['temperature']
                if type(temperature)!=np.int64 and len(temperature)>1:
                    temperature=temperature[0]
                    last_good_temperature = temperature
    
    
    
            temp_temps.append(temperature)
        
        
        to_write_train.append([i+time_offset,get_average_temperature(temp_temps)+temperature_offset])

    to_write_validate = []
    for i in range(event_validate_start,event_validate_end):
        temp_temps = []
        last_good_temperature =-1
        for time in event_to_time[i]:
            ind = home_index[time]
            ind = timestamp_to_hour(ind)
            
            if ind not in temperatures.index:
                
                temperature = last_good_temperature
            
            #print(index)
            else:
                
                temperature = temperatures.ix[ind]['temperature']
                if type(temperature)!=np.int64 and len(temperature)>1:
                    temperature=temperature[0]
                    last_good_temperature = temperature

            temp_temps.append(temperature)
        
        to_write_validate.append([i+time_offset,get_average_temperature(temp_temps)+temperature_offset])

    wf.write_a_psl_file(write_dir,'Temperature_train',to_write_train)
    wf.write_a_psl_file(write_dir,'Temperature_test',to_write_validate)

def write_json(write_dir,filename,file_content):
    with open('{}{}.json'.format(write_dir,filename),'w') as f:
        json.dump(file_content,f)

def write_config_files(write_dir,offsets,num_daytimes,num_days,num_temperatures):
    days = [i+offsets['days_offset'] for i in range(num_days)]
    daytimes = [i+offsets['hours_offset'] for i in range(num_daytimes)]
    temperatures = [i+offsets['temperature_offset'] for i in range(num_temperatures)]
    temp_dict ={'temperatures':temperatures}
    write_json(write_dir,'temperatures',temp_dict)
    write_json(write_dir,'daytimes',{'daytimes':daytimes})
    write_json(write_dir,'days',{'days':days})


def write_context_interval(home,home_index,offsets,new_event_dictionary,write_dir,num_daytimes,num_days,num_temperatures):
    
    write_hours_interval(home,new_event_dictionary['etti'],offsets['time_offset'],offsets['hours_offset'],0,new_event_dictionary['event_train'],new_event_dictionary['event_train'],new_event_dictionary['event_validate'],write_dir)
    write_days_interval(home,new_event_dictionary['etti'],offsets['time_offset'],offsets['days_offset'],0,new_event_dictionary['event_train'],new_event_dictionary['event_train'],new_event_dictionary['event_validate'],write_dir)
    write_temperature_interval(home.index,offsets['time_offset'],offsets['temperature_offset'],new_event_dictionary['ett'],0,new_event_dictionary['event_train'],new_event_dictionary['event_train'],new_event_dictionary['event_validate'],write_dir,'../data/threshold_settings/')

    write_config_files(write_dir,offsets,num_daytimes,num_days,num_temperatures)



def write_context_instance(home,home_index,offsets,train_start,train_end,validate_start,validate_end,write_dir,num_daytimes,num_days,num_temperatures):

    write_hours_instance(home_index,offsets['time_offset'],offsets['hours_offset'],train_start,train_end,validate_start,validate_end,write_dir)
    write_days_instance(home_index,offsets['time_offset'],offsets['days_offset'],train_start,train_end,validate_start,validate_end,write_dir)
    write_temperature_instance(home_index,offsets['time_offset'],offsets['temperature_offset'],train_start,train_end,validate_start,validate_end,write_dir,'../data/threshold_settings/')
    write_config_files(write_dir,offsets,num_daytimes,num_days,num_temperatures)

