
def instances_to_intervals(switch,readings):
    uid = 0
    to_return = [uid]
    
    for i in range(1,len(readings)):
        if abs(readings[i]-readings[i-1])>switch:
            uid = uid+1
        to_return.append(uid)
    return to_return


def get_duration_id(duration,threshold_dict):
    
    for k,v in threshold_dict.items():
        if duration>v[0] and duration<=v[1]:
            return k
    else:
        print(duration)

def get_durations(states,key):
    all_sequences = []
    new_sequence=[]
    for si in range(len(states)):
        if states[si]==key:
            new_sequence.append(si)
        
        else:
            if len(new_sequence)>0:
                all_sequences.append(new_sequence)
            new_sequence=[]

    if si==len(states)-1 and len(new_sequence)>0:
        all_sequences.append(new_sequence)
    
    return all_sequences


def map_to_duration(time_spans,threshold_dict):
    to_return = []
    
    for t in time_spans:
        duration = len(t)
        duration_id = get_duration_id(duration,threshold_dict)
        for i in t:
            to_return.append(duration_id)
    return to_return

def to_write_duration_from_events(events,threshold_dict):
    
    to_return = {}
    
    for event,times in events.items():
        duration = get_duration_id(len(times),threshold_dict)
        to_return[event]=duration
    return to_return

def get_events_with_time_index(readings,threshold,home_index):
    
    events = instances_to_intervals(threshold,readings)
    #print(len(events)==len(total_power))
    event_to_times = {}
    time_to_event={}
    index_to_event = {}
    event_to_time_indices = {}
    
    for e_i in range(len(events)):
        e = events[e_i]
        if e not in event_to_times:
            event_to_times[e]=[]
            event_to_time_indices[e]=[]
        event_to_times[e].append(e_i)
        index_to_event[home_index[e_i]]=e
        event_to_time_indices[e].append(home_index[e_i])
        time_to_event[e_i]=e
    return {'tte':time_to_event,'ett':event_to_times,'ite':index_to_event,'etti':event_to_time_indices,'check':events}

def get_train_test_ids(event_dictionary,train_index,validate_index):

    map_old_event_to_new_event={}


    for time in range(train_index):
        map_old_event_to_new_event[time]=event_dictionary['tte'][time]
    
    last_train_event = event_dictionary['tte'][train_index-1]
    first_validate_event = event_dictionary['tte'][train_index]

    val_offset=0
    if last_train_event==first_validate_event:
        val_offset = 1
        
    for time in range(train_index,validate_index):
            map_old_event_to_new_event[time]=event_dictionary['tte'][time]+val_offset

    return map_old_event_to_new_event

def get_test_offset(event_dictionary,train_index):
    last_train_event = event_dictionary['tte'][train_index-1]
    first_validate_event = event_dictionary['tte'][train_index]
    

    return int(last_train_event==first_validate_event)

def get_new_event_lookup(event_dictionary,train_index,validate_index,home_index):
    
    map_old_event_to_new_event={'tte':{},'ett':{},'etti':{},'ite':{}}
    
    
    for time in range(train_index):
        map_old_event_to_new_event['tte'][time]=event_dictionary['tte'][time]
        
        map_old_event_to_new_event['ite'][home_index[time]]=event_dictionary['tte'][time]
        
        event = event_dictionary['tte'][time]
        times = event_dictionary['ett'][event]
        time_indices = event_dictionary['etti'][event]
        #
        map_old_event_to_new_event['ett'][event]=[t for t in times ]
        map_old_event_to_new_event['etti'][event]=[t for t in time_indices ]
    
    last_train_event = event_dictionary['tte'][train_index-1]
    first_validate_event = event_dictionary['tte'][train_index]

    val_offset=int(last_train_event==first_validate_event)

    

    for time in range(train_index,validate_index):
        map_old_event_to_new_event['tte'][time]=event_dictionary['tte'][time]+val_offset
        map_old_event_to_new_event['ite'][home_index[time]]=event_dictionary['tte'][time]+val_offset
    
        event = event_dictionary['tte'][time]
        times = event_dictionary['ett'][event]
        time_indices=event_dictionary['etti'][event]
        

        map_old_event_to_new_event['ett'][event+val_offset]=[t for t in times ]
        map_old_event_to_new_event['etti'][event+val_offset]=[t for t in time_indices ]
    
    map_old_event_to_new_event['event_train']=map_old_event_to_new_event['tte'][train_index]
    
    if validate_index not in event_dictionary['tte']:
  
        map_old_event_to_new_event['event_validate']=map_old_event_to_new_event['tte'][validate_index-1]+1

    else:

        map_old_event_to_new_event['event_validate']=event_dictionary['tte'][validate_index]+val_offset
        
       
        if event_dictionary['tte'][validate_index]==event_dictionary['tte'][validate_index-1]:
    
            map_old_event_to_new_event['event_validate']=map_old_event_to_new_event['event_validate']+1
    

    
    return map_old_event_to_new_event

def adjust_times(times,train_index):
    return [t for t in times if t<train_index]

def get_new_event_lookup_cut_off(event_dictionary,train_index,validate_index,home_index):
    
    map_old_event_to_new_event={'tte':{},'ett':{},'etti':{},'ite':{}}
    
    val_offset=0
    for time in range(train_index):
        map_old_event_to_new_event['tte'][time]=event_dictionary['tte'][time]
        
        map_old_event_to_new_event['ite'][home_index[time]]=event_dictionary['tte'][time]
        
        event = event_dictionary['tte'][time]
        times = event_dictionary['ett'][event]
        time_indices = event_dictionary['etti'][event]
        
        map_old_event_to_new_event['ett'][event]=[t for t in times if t<train_index]
        map_old_event_to_new_event['etti'][event]=[time_indices[t] for t in range(len(time_indices)) if times[t] < train_index]
    
    if event_dictionary['tte'][train_index]==event_dictionary['tte'][train_index-1]:
        map_old_event_to_new_event['event_train']=map_old_event_to_new_event['tte'][train_index-1]+1
        val_offset=1


    
    
    
    for time in range(train_index,validate_index):
        map_old_event_to_new_event['tte'][time]=event_dictionary['tte'][time]+val_offset
        map_old_event_to_new_event['ite'][home_index[time]]=event_dictionary['tte'][time]+val_offset
        
        event = event_dictionary['tte'][time]
        times = event_dictionary['ett'][event]
        time_indices=event_dictionary['etti'][event]
        
        
        map_old_event_to_new_event['ett'][event+val_offset]=[t for t in times if t>=train_index]
        map_old_event_to_new_event['etti'][event+val_offset]=[time_indices[t] for t in range(len(time_indices)) if times[t] >= train_index]

    map_old_event_to_new_event['event_train']=map_old_event_to_new_event['tte'][train_index]


    if validate_index not in event_dictionary['tte']:
        
        map_old_event_to_new_event['event_validate']=map_old_event_to_new_event['tte'][validate_index-1]+1

    else:
    
        map_old_event_to_new_event['event_validate']=event_dictionary['tte'][validate_index]+val_offset
        
        
        if event_dictionary['tte'][validate_index]==event_dictionary['tte'][validate_index-1]:
            
            map_old_event_to_new_event['event_validate']=map_old_event_to_new_event['event_validate']+1



    return map_old_event_to_new_event
