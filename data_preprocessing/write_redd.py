import write_interval
import write_instance
from process_raw_data import process_dataset
import processing_duration
import pandas as pd
import pickle
import data_formatting as df
import numpy as np
import write_functions as wf
import operator
import os
import sys


def round_up_to_nearest_ten(value):
    temp = int(value/10)
    return temp*10+10


def get_offsets(offset_info):
    
    
    app_offset = 0
    
    duration_offset = round_up_to_nearest_ten(app_offset+offset_info['apps'])
    

    
    combo_offset = round_up_to_nearest_ten(offset_info['durations']+duration_offset)
    
    
    
    difference_offset = round_up_to_nearest_ten(offset_info['combos']+combo_offset)
    
    
    
    time_offset = round_up_to_nearest_ten(offset_info['max_difference']*2+difference_offset)
    
    return {'time_offset':time_offset,'difference_offset':difference_offset,'combo_offset':combo_offset,\
        'duration_offset':duration_offset,'app_offset':app_offset}



def prepare_home(home_id,year):
    home = get_home(home_id)
    
    home = home_to_year(home,year,1)
    
    months =  home.groupby(lambda x:x.month).groups
    
    return months


def get_event_threshold(customs):
    #print(customs)
    sort_by_means = {app:dist['mu'] for app,dist in customs.items()}
    sorted_means =   sorted(sort_by_means.items(), key=operator.itemgetter(1))
    smallest_app = sorted_means[0][0]
    if smallest_app==' MICR' or smallest_app==' DISH' :
        smallest_app=sorted_means[1][0]
    #print(smallest_app)
    return customs[smallest_app]['mu']-customs[smallest_app]['sigma']

def write_home(home_id, write_type,train):
    target_apps =  [' MICR',' DISH',' LITE',' REFG']
    
    if int(home_id)==6:
        del target_apps[0]
        del target_apps[0]
    
    dir_path = '../data/redd'
    experiment_dir = '../data/temp_testing'

    to_evalute_directory = '../data/to_evaluate/redd'

    use_column = ' MAIN'
    
    with open('../data/threshold_settings/duration_dict_redd.pkl','rb') as f:
        duration_dict = pickle.load(f)

    offset_info = {'apps':len(target_apps),'durations':len(duration_dict),\
    'combos':2**len(target_apps),'max_difference': 16000}

    offsets = get_offsets(offset_info)
    
    offsets['max_difference']= offset_info['max_difference']

    home = process_dataset.get_home_redd(home_id,'../data/original_data/clean_redd')
##make global home parameters (for year or not)



##with ghost noise

    home = process_dataset.adjust_total_power_no_noise(home,target_apps,use_column)
    
    stats_path ='{}stats.pkl'.format('../data/consumption_stats/redd/train/home_{}/'.format(home_id))


    if os.path.isfile(stats_path):
        
        with open(stats_path,'rb') as f:
            customs = pickle.load(f)

    else:
        customs = process_dataset.get_custom_normal_training(home,target_apps,20,0,int(len(home[use_column])/2))
    
        with open(stats_path,'wb') as f:
            pickle.dump(customs,f)
    


    ghost_noise = min(home[use_column])

#customs = get_experiment_settings(experiment_dir,'home_{}_custom_app_distributions'.format(home_id))

##want to make smaller and look at sensitivity of results to this parameter

    event_threshold = get_event_threshold(customs)
        #if customs[' LITE']['mu']<customs[' REFG']['mu']:
        #event_threshold = int(customs[' LITE']['mu']-customs[' LITE']['sigma'])
        #else:
#event_threshold = int(customs[' REFG']['mu']-customs[' REFG']['sigma'])
    
    ##refrigerator or lite
    #int(customs['refrigerator1']['mu']-customs['refrigerator1']['sigma'])

    readings =home[use_column]
    
    
    write_dir = '{}/{}/{}/home_{}'.format(dir_path,write_type,train,home_id)
    evaluate_dir = '{}/{}/{}/home_{}'.format(to_evalute_directory,write_type,train,home_id)
    results_dir = '../data/results/redd/{}/{}/home_{}'.format(write_type,train,home_id)

        ##make write directory
    if not os.path.exists(write_dir):
        os.makedirs(write_dir)
        
    write_dir = '{}/'.format(write_dir)

    if not os.path.exists(evaluate_dir):
        os.makedirs(evaluate_dir)
    
    evaluate_dir = '{}/'.format(evaluate_dir)


    if not os.path.exists(results_dir):
        os.makedirs(results_dir)
    
    results_dir = '{}/'.format(results_dir)

    with open('{}offset_dict.pkl'.format(evaluate_dir),'wb') as f:
        pickle.dump(offsets,f)

    with open('{}appliance_lookup.pkl'.format(evaluate_dir),'wb') as f:
        pickle.dump({i:target_apps[i] for i in range(len(target_apps))},f)
    
    with open('{}stats.pkl'.format(evaluate_dir),'wb') as f:
        pickle.dump(customs,f)


#write_interval.write_interval(offsets,home,readings,event_threshold,use_column,customs,'validate',ghost_noise,target_apps,duration_dict,write_dir)

    if write_type=='interval':
        write_interval.write_interval(offsets,home,readings,event_threshold,use_column,customs,train,ghost_noise,target_apps,duration_dict,write_dir,False,4,evaluate_dir)
    else:
        write_instance.write_instance(offsets,home,readings,event_threshold,use_column,customs,train,ghost_noise,target_apps,write_dir,False,evaluate_dir)


        ##test is added here
##need to change this line and do redd version
#write_all_month(month_df,target_apps,write_dir,experiment_dir,home_id,month_id,event_threshold)

if __name__ == "__main__":
    
    home = sys.argv[1]
    interval  =sys.argv[2]
    train = sys.argv[3]

    write_home(home,interval,train)

