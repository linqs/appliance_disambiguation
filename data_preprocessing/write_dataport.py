import write_interval
from process_raw_data import process_dataset
import processing_duration
import pandas as pd
import pickle
import data_formatting as df
import numpy as np
import write_functions as wf
import operator
import os
import write_instance
import sys



def round_up_to_nearest_ten(value):
    temp = int(value/10)
    return temp*10+10


def get_offsets(offset_info,context):
    
    to_return = {}
    
    app_offset = 0
    
    duration_offset = round_up_to_nearest_ten(app_offset+offset_info['apps'])
    
    if context:
    
    #durations = get_experiment_settings(experiment_directory,'duration')
    
    ##insert temperature
    ##RIGHT NOW ITS JUST THREE COULD LOAD IN PERCENTILES
        temperature_offset = round_up_to_nearest_ten(offset_info['durations']+duration_offset)
    
    
    
        days_offset =  round_up_to_nearest_ten(offset_info['temperatures']+temperature_offset)
    
    
    
        hours_offset = round_up_to_nearest_ten(offset_info['days']+days_offset)
    
    
    
    
        combo_offset = round_up_to_nearest_ten(offset_info['hours']+hours_offset)
    
    
    
        difference_offset = round_up_to_nearest_ten(offset_info['combos']+combo_offset)
    
    
    
        time_offset = round_up_to_nearest_ten(offset_info['max_difference']*2+difference_offset)
    
        return {'time_offset':time_offset,'difference_offset':difference_offset,'combo_offset':combo_offset,\
        'duration_offset':duration_offset,'app_offset':app_offset,'days_offset':days_offset,'hours_offset':hours_offset,'temperature_offset':temperature_offset}
        
    combo_offset = round_up_to_nearest_ten(offset_info['durations']+duration_offset)
        
        
        
    difference_offset = round_up_to_nearest_ten(offset_info['combos']+combo_offset)
        
        
        
    time_offset = round_up_to_nearest_ten(offset_info['max_difference']*2+difference_offset)
    
    return {'time_offset':time_offset,'difference_offset':difference_offset,'combo_offset':combo_offset,\
        'duration_offset':duration_offset,'app_offset':app_offset}



def prepare_home(home_id,year):
    home = get_home(home_id)
    
    home = home_to_year(home,year,1)
    
    months =  home.groupby(lambda x:x.month).groups
    
    return months


def write_home(home_id,write_type,context,train,time_resolution):
    target_apps = ['air1', 'furnace1', 'refrigerator1',  'clotheswasher1','drye1','dishwasher1', 'kitchenapp1', 'microwave1']
    dir_path = '../data/dataport'
    
    to_evalute_directory = '../data/to_evaluate/dataport'
    

    
    experiment_dir = '../data/temp_testing'
    context = int(context)

    ##if using thresholds
    with open('../data/threshold_settings/duration_dict_dataport_{}.pkl'.format(time_resolution),'rb') as f:
        duration_dict = pickle.load(f)
    
    #with open('../data/dataport/durations_dpgmm.pkl'.format(home_id),'rb') as f:
    #    duration_dpgmm = pickle.load(f)
    
    #num_durations = len([w for w in duration_dpgmm.weights_ if w>.001])

    offset_info = {'apps':len(target_apps),'durations':len(duration_dict),\
    'combos':2**len(target_apps),'max_difference': 16000,'days':7,'hours':24,'temperatures':3}

    offsets = get_offsets(offset_info,context)

    offsets['max_difference']= offset_info['max_difference']
    home = process_dataset.get_home_dataport(home_id,'../data/original_data/dataport_HDF5',time_resolution)
##make global home parameters (for year or not)
    home = home*1000

    use_column='use'
    home = process_dataset.adjust_total_power_no_noise(home,target_apps,use_column)
    
    stats_path ='{}stats.pkl'.format('../data/consumption_stats/dataport/train/minute/home_{}/'.format(home_id))
 

        


    if os.path.isfile(stats_path):
            
        with open(stats_path,'rb') as f:
            customs = pickle.load(f)

    else:

        os.makedirs('../data/consumption_stats/dataport/train/{}/home_{}/'.format(temporal_resolution,home_id))


            
        customs = process_dataset.get_custom_normal_training(home,target_apps,20,0,int(len(home[use_column])/2))
        

    
        with open(stats_path,'wb') as f:
               pickle.dump(customs,f)

    ghost_noise = min(home['use'])
  
#customs = get_experiment_settings(experiment_dir,'home_{}_custom_app_distributions'.format(home_id))

##want to make smaller and look at sensitivity of results to this parameter
    event_threshold = int(customs['refrigerator1']['mu']-customs['refrigerator1']['sigma'])




##group to months
    home = process_dataset.home_to_year(home,2015,1)

    months =  home.groupby(lambda x:x.month).groups


    for month_id,month_indices in months.items():
        month_df = home.loc[month_indices[0]:month_indices[-1]]

        write_dir = '{}/{}/{}/{}/home_{}/month_{}'.format(dir_path,write_type,train,temporal_resolution,home_id,month_id)



        evaluate_dir = '{}/{}/{}/{}/home_{}/month_{}'.format(to_evalute_directory,write_type,train,temporal_resolution,home_id,month_id)
        results_dir = '../data/results/dataport/{}/{}/{}/home_{}/month_{}'.format(write_type,train,temporal_resolution,home_id,month_id)
        
        ##make write directory
        if not os.path.exists(write_dir):
            os.makedirs(write_dir)
        
        if not os.path.exists(evaluate_dir):
            os.makedirs(evaluate_dir)
    
        evaluate_dir = '{}/'.format(evaluate_dir)



        if not os.path.exists(results_dir):
            os.makedirs(results_dir)
    
        results_dir = '{}/'.format(results_dir)



        with open('{}offset_dict.pkl'.format(evaluate_dir),'wb') as f:
            pickle.dump(offsets,f)
    
        with open('{}appliance_lookup.pkl'.format(evaluate_dir),'wb') as f:
            pickle.dump({i:target_apps[i] for i in range(len(target_apps))},f)

#UNCOMMENT ALL
        write_dir = '{}/'.format(write_dir)

        readings = month_df['use']
        if write_type=='interval':
            write_interval.write_interval(offsets,month_df,readings,event_threshold,use_column,customs,train,ghost_noise,target_apps,duration_dict,write_dir,context,len(duration_dict),evaluate_dir)
        else:
            write_instance.write_instance(offsets,month_df,readings,event_threshold,use_column,customs,train,ghost_noise,target_apps,write_dir,context,evaluate_dir)

if __name__ == "__main__":
    home = sys.argv[1]
    interval  =sys.argv[2]
    context =sys.argv[3]
    train =sys.argv[4]
    temporal_resolution =sys.argv[5]
    write_home(home,interval,context,train,temporal_resolution)

