from process_raw_data import process_dataset
import processing_duration
import pandas as pd
import pickle
import data_formatting as df
import numpy as np
import write_functions as wf
import operator
import write_context

def to_write_is_on_all_train(home,ons,target_apps,offset,app_offset):
    #ons = wrf.get_ons_home(home,customs,target_apps)
    app_ids = {target_apps[i]:i for i in range(len(target_apps))}
    to_return_d={}
    to_return = []
    
    temp = list(ons.values())
    temp = temp[0]
    for t in range(len(temp)):
        for k,v in ons.items():
            to_return.append([t+offset,app_ids[k]+app_offset,ons[k][t]])
        to_return_d[k]=to_return
    return to_return

def write_ons(home,customs,target_apps,train_index,validate_index,app_offset,time_offset,write_dir,to_evaluate_directory):
    
    ons = df.get_ons_home_modified(home,customs,target_apps)
    
    
    
    with open('{}on_dictionary.pkl'.format(to_evaluate_directory),'wb') as f:
        pickle.dump(ons,f)
    
    write_train = to_write_is_on_all_train(home,ons,target_apps,time_offset,app_offset)
    
    wf.write_a_psl_file(write_dir,'IsOn_train',write_train[:train_index*len(target_apps)])

    wf.write_a_psl_file(write_dir,'IsOn_train_target',[[t[0],t[1]] for t in write_train[:train_index*len(target_apps)]])
    
    wf.write_a_psl_file(write_dir,'IsOn_test',[[t[0],t[1]] for t in write_train[train_index*len(target_apps):validate_index*len(target_apps)]])
    
    wf.write_a_psl_file(write_dir,'IsOn_test_target',[[t[0],t[1]] for t in write_train[train_index*len(target_apps):validate_index*len(target_apps)]])
    
    return ons

def to_write_combo(series_to_write,n,time_offset,combo_offset):
    
    to_return = []
    
    for t in range(len(series_to_write)):
        for app in range(n):
            on = 0
            if series_to_write[t]==app:
                on=1
            to_return.append([t+time_offset,app+combo_offset,on])
    return to_return

def write_combos(customs,target_apps,readings,ons,time_offset,combo_offset,train_index,validate_index,write_dir,to_evaluate_directory):
    new_customs = {i:customs[target_apps[i]] for i in range(len(target_apps))}
    
    combos = df.get_total_energy_clusters(readings,new_customs,target_apps)
    
    combo_dict = df.get_actual_combos(readings,customs,ons,new_customs,target_apps)
    
    ##move to data functions possibly?
    combos_to_write=to_write_combo(combo_dict['real_combos'],2**len(target_apps),time_offset,combo_offset)
    
    
    wf.write_a_psl_file(write_dir,'Combo_train',combos_to_write[:train_index*2**len(target_apps)])
    wf.write_a_psl_file(write_dir,'Combo_train_target',[[i[0],i[1]] for i in combos_to_write[:train_index*2**len(target_apps)]])
    
    wf.write_a_psl_file(write_dir,'Combo_test',[[t[0],t[1]] for t in combos_to_write[train_index*2**len(target_apps):validate_index*2**len(target_apps)]])
    
    wf.write_a_psl_file(write_dir,'Combo_test_target',[[t[0],t[1]] for t in combos_to_write[train_index*2**len(target_apps):validate_index*2**len(target_apps)]])
    
    with open('{}/combo_dict.pkl'.format(to_evaluate_directory),'wb') as f:
        pickle.dump(combo_dict,f)
    
    with open('{}/combo_dict_other.pkl'.format(to_evaluate_directory),'wb') as f:
        pickle.dump(combos,f)
    
    return combo_dict

def to_write_closeness_to_total_energy_instances(readings,combo_dict,combo_offset,ghost_noise):
    
    to_return = {}
    
    for time in range(len(readings)):
        this_energy = readings[time]
        probs = df.simple_probability(this_energy,combo_dict['buckets'],ghost_noise)
        temp = []
        for p_i in range(len(probs)):
            temp.append([combo_offset+p_i,probs[p_i]])
        
        to_return[time]=temp
    
    return to_return

def prune_keeping_probs(dict_form_probs,top_k,threshold):
    to_return = {}
    
    for k,v in dict_form_probs.items():
        sorted_probs = sorted(v.items(), key=operator.itemgetter(1),reverse=True)
        greater_than_threshold = [[k,vv] for k,vv in v.items() if vv>threshold]
        tops = [[ii for ii in i] for i in sorted_probs[:top_k]]
        
        if len(tops)>len(greater_than_threshold):
            to_return[k]=tops
        else:
            to_return[k]=greater_than_threshold
    return to_return

def times_probs(combo_probs):
    to_return = {}
    for entry,thing in combo_probs.items():
        time_index = entry
        for t in thing:
            combo_id = t[0]
            probability = t[1]
            
            if time_index not in to_return:
                to_return[time_index]={}
            to_return[time_index][combo_id]=probability
    return to_return

def prune_dict_to_file(pruned,event_start,event_stop,time_offset):
    to_return = []
    
    for event in range(event_start,event_stop):
        for reading in pruned[event]:
            to_return.append([event+time_offset,reading[0],reading[1]])
    return to_return

def write_close_to_consumption(readings,combo_dict,combo_offset,time_offset,ghost_noise,train_index,validate_index,write_dir):
    
    
    closeness_lookup = to_write_closeness_to_total_energy_instances(readings,\
                                                                    combo_dict,combo_offset,ghost_noise)
        
    dict_form = times_probs(closeness_lookup)
    pruned = prune_keeping_probs(dict_form,3,.5)
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
    write_train = prune_dict_to_file(pruned,0,train_index,time_offset)
    write_test = prune_dict_to_file(pruned,train_index,validate_index,time_offset)
                                                                    
                                                                    #train_validate_closeness = split_train_validate_test_list_val(event_to_time,pruned,event_train,event_validate)
                                                                    
                                                                    #closetoconsumption_train= [[i[0]+time_offset,i[1],i[2]] for i in  train_validate_closeness['train']]
                                                                    #closetoconsumption_validate = [[i[0]+time_offset,i[1],i[2]] for i in  train_validate_closeness['validate']]
                                                                    
                                                                    #df.write_a_file(write_dir,'CloseToConsumption_train',closetoconsumption_train)
                                                                    #df.write_a_file(write_dir,'CloseToConsumption_validate',closetoconsumption_validate)
    wf.write_a_psl_file(write_dir,'CloseToConsumption_train',write_train)
                                                                    
    wf.write_a_psl_file(write_dir,'CloseToConsumption_test',write_test)
                                                                    
                                                                    
    return pruned



def pruned_to_apps(combo_offset,time_dict,combo_lookup,target_apps,app_offset,app_ids):
    
    to_return = {}
    
    for time,combos in time_dict.items():
        temp = []
        for c in combos:
            combo_index = c-combo_offset
            apps = combo_lookup[combo_index]
            for a in apps:
                app_id = app_ids[a]+app_offset
                if app_id not in temp:
                    temp.append(app_id)
        to_return[time]=temp
    return to_return

##Think can move over
def write_candidates(pruned,combo_offset,time_offset,app_offset,combo_lookup,target_apps,train_start,train_end,validate_start,validate_end,write_dir):
    
    app_ids = {i:target_apps[i] for i in range(len(target_apps))}
    
    ##Write candidate apps
    apps_to_test=pruned_to_apps(combo_offset,{k:[vv[0] for vv in v] for k,v in pruned.items()},combo_lookup,target_apps,app_offset,{v:k for k,v in app_ids.items()})
    
    apps_to_infer_train = []
    for k in range(train_start,train_end):
        temp = apps_to_test[k]
        for vv in temp:
            apps_to_infer_train.append([k+time_offset,vv])

    apps_to_infer_validate = []
    for k in range(validate_start,validate_end):
        temp = apps_to_test[k]
        for vv in temp:
            apps_to_infer_validate.append([k+time_offset,vv])

    wf.write_a_psl_file(write_dir,'Candidate_apps_train',apps_to_infer_train)
    wf.write_a_psl_file(write_dir,'Candidate_apps_test',apps_to_infer_validate)
    
    
    ##Write candidate combos
    combos_to_infer_train = []
    for k in range(train_start,train_end):
        for combo_val in pruned[k]:
            
            combos_to_infer_train.append([k+time_offset,combo_val[0]])

    combos_to_infer_validate = []
    for k in range(validate_start,validate_end):
        for combo_val in pruned[k]:
            combos_to_infer_validate.append([k+time_offset,combo_val[0]])

    wf.write_a_psl_file(write_dir,'Candidate_combos_train',combos_to_infer_train)
    wf.write_a_psl_file(write_dir,'Candidate_combos_test',combos_to_infer_validate)



def get_distance_to_difference(mean,difference):
    difference = abs(difference)
    distance = abs(mean-difference)
    normalized = distance/max([mean,difference])
    if normalized>1:
        return 1
    elif normalized<0:
        return 0
    else:
        return 1-normalized


def get_all_distances(differences,customs,new_app_ids):
    to_return = []
    
    for app,stats in customs.items():
        mean = stats['mu']
        for difference in differences:
            distance = get_distance_to_difference(mean,difference[2])
            app_id = new_app_ids[app]
            to_return.append([app_id,difference[2],distance])
    return to_return

def merge_distances(train_distances,validate_distances,diff_min):
    seen = {}
    to_return =[]
    for distance in train_distances:
        key = distance[1]+diff_min
        value = distance[2]
        if key not in seen:
            seen[key]=[]
        if value not in seen[key]:
            to_return.append(distance)
        seen[key].append(value)
    
    for distance in validate_distances:
        key = distance[1]+diff_min
        value = distance[2]
        if key not in seen:
            seen[key]=[]
        if value not in seen[key]:
            to_return.append(distance)
        seen[key].append(value)
    
    return to_return

def merge_positives(train_positives,validate_positives,diff_min):
    seen = {}
    to_return =[]
    for distance in train_positives:
        key = int(distance[0]+diff_min)
        value = int(distance[1])
        if key not in seen:
            seen[key]=[]
        if value not in seen[key]:
            to_return.append(distance)
        seen[key].append(value)
    
    for distance in validate_positives:
        key = int(distance[0]+diff_min)
        value = int(distance[1])
        if key not in seen:
            seen[key]=[]
        if value not in seen[key]:
            to_return.append(distance)
        seen[key].append(value)
    
    return to_return

def get_differences_from_instances(start,stop,readings):
    
    to_return = []
    
    for time in range(start,stop-1):
        last_power = readings[time]
        first_power = readings[time+1]
        diff = int(first_power - last_power)
        to_return.append([time,time+1,diff])
    
    return to_return

#def get_all_distances(differences,customs,app_ids):
#    to_return = []
#    new_app_ids = {v:k for k,v in app_ids.items()}
#    for app,stats in customs.items():
#        mean = stats['mu']
#        for difference in differences:
#            distance = get_distance_to_difference(mean,difference[2])
#            app_id = new_app_ids[app]
#            to_return.append([app_id,difference[2],distance])
#    return to_return

def all_positive_differences(differences):
    return [[i[2],int(i[2]>0) ] for i in differences ]

def write_differences(train_start,train_end,validate_start,validate_end,readings,customs,target_apps,diff_max,app_offset,diff_offset,time_offset,write_dir):
    
    train_differences = get_differences_from_instances(train_start,train_end,readings)
    validate_differences = get_differences_from_instances(validate_start,validate_end,readings)
    
    new_app_ids = {target_apps[i]:i for i in range(len(target_apps))}
    
    train_distances = get_all_distances(train_differences,customs,new_app_ids)
    validate_distances = get_all_distances(validate_differences,customs,new_app_ids)
    
    
    all_distances = merge_distances(train_distances,validate_distances,diff_max)
    
    is_positive = merge_positives(all_positive_differences(train_differences),all_positive_differences(validate_differences),diff_max)
    
    to_write_distances =[[i[0]+app_offset,i[1]+diff_max,i[2]] for i in all_distances]
    to_write_differences_train = [[i[0]+time_offset,i[1]+time_offset,i[2]+diff_offset+diff_max] for i in train_differences]
    to_write_differences_validate = [[i[0]+time_offset,i[1]+time_offset,i[2]+diff_offset+diff_max] for i in validate_differences]
    to_write_positive = [[i[0]+diff_max,i[1]] for i in is_positive]
    
    wf.write_a_psl_file(write_dir,'Differences_train',to_write_differences_train)
    wf.write_a_psl_file(write_dir,'Differences_test',to_write_differences_validate)
    wf.write_a_psl_file(write_dir,'Distances',to_write_distances)
    wf.write_a_psl_file(write_dir,'Positive',to_write_positive)




def write_toggles(target_apps,combo_offset,app_offset,write_dir,combos):
    toggles = df.get_toggle(target_apps,combo_offset,app_offset,combos)
    wf.write_a_psl_file(write_dir,'Toggle',toggles)

def write_precedes(train_start,train_end,validate_start,validate_end,time_offset,write_dir):
    precedes_train = df.get_precedes(train_start,train_end,time_offset)
    precedes_validate = df.get_precedes(validate_start,validate_end,time_offset)
    
    wf.write_a_psl_file(write_dir,'Precedes_train',precedes_train)
    wf.write_a_psl_file(write_dir,'Precedes_test',precedes_validate)

def write_appliance(app_offset,target_apps,write_dir):
    wf.write_a_psl_file(write_dir,'Appliances',[[a+app_offset,a+app_offset] for a in range(len(target_apps))])

def write_combo(combo_offset,target_apps,write_dir):
    wf.write_a_psl_file(write_dir,'Combos',[[c+combo_offset,c+combo_offset] for c in range(2**len(target_apps))])

def write_aic(target_apps,combo_lookup,app_offset,combo_offset,write_dir):
    aic = df.to_write_active_in_combo_new(target_apps,combo_lookup,app_offset,combo_offset)
    wf.write_a_psl_file(write_dir,'ActiveInCombo',aic)

def write_config_files(write_dir,offsets,num_apps):
    apps = [i+offsets['app_offset'] for i in range(num_apps)]
    write_context.write_json(write_dir,'appliances',{'appliances':apps})

def write_use(home,target_apps,start,stop,to_evaluate_directory):
    to_return = {}
    for app in target_apps:
        to_return[app]=home[app][start:stop]

    with open('{}use_lookup.pkl'.format(to_evaluate_directory),'wb') as f:
        pickle.dump(to_return,f)

def write_instance(offsets,home,readings,event_threshold,use_column,customs,train_condition,ghost_noise,target_apps,write_dir,context,to_evaluate_directory):
    ##initial things are needed like offsets
    ##events event map
    
    
    if train_condition=='validate':
        train_index =  int(len(home[use_column])/2)
        validate_index = int(len(home[use_column])/2 + len(home[use_column])/4)
    else:
        train_index = int(len(home[use_column])/2 + len(home[use_column])/4)
        validate_index = len(home[use_column])
    
    home_index = home.index

    with open('{}test_indices.pkl'.format(to_evaluate_directory),'wb') as f:
        pickle.dump({'start':train_index,'stop':validate_index},f)

    
    if context:
        write_context.write_context_instance(home,home_index,offsets,0,train_index,train_index,validate_index,write_dir,24,7,4)

    write_config_files(write_dir,offsets,len(target_apps))
    write_use(home,target_apps,train_index,validate_index,to_evaluate_directory)

##Write Ons
    ons  = write_ons(home,customs,target_apps,train_index,validate_index,offsets['app_offset'],offsets['time_offset'],write_dir,to_evaluate_directory)
    
    ##Write Combos
    combo_dict = write_combos(customs,target_apps,home[use_column],ons,offsets['time_offset'],offsets['combo_offset'],train_index,validate_index,write_dir,to_evaluate_directory)
    
    ##Close to Consumption
    pruned = write_close_to_consumption(home[use_column],combo_dict,offsets['combo_offset'],offsets['time_offset'],ghost_noise,train_index,validate_index,write_dir)
    
    ##Write Candidates
    candidates = write_candidates(pruned,offsets['combo_offset'],offsets['time_offset'],\
                                  offsets['app_offset'],combo_dict['combo_lookup'],target_apps,\
                                  0,train_index,train_index,validate_index,\
                                  write_dir)
        
        
        
        
        
                                  ##Write Differences
                                  
    write_differences( 0,train_index,train_index,validate_index,\
                                                    home[use_column],customs,target_apps,offsets['max_difference'],\
                                                    offsets['app_offset'],offsets['difference_offset'],offsets['time_offset'],write_dir)
                                  
                                  
                                  ##Write Toggle
    write_toggles(target_apps,offsets['combo_offset'],offsets['app_offset'],write_dir,combo_dict['combo_lookup'])
                                  
                                  ##Write Precedes 
    write_precedes(0,train_index,train_index,validate_index,offsets['time_offset'],write_dir)
                                  
                                  
                                  ##Write Appliance
    write_appliance(offsets['app_offset'],target_apps,write_dir)
                                  
                                  ##Write Combo
    write_combo(offsets['combo_offset'],target_apps,write_dir)
                                  
                                  ##Write ActiveInCombo
    write_aic(target_apps,combo_dict['combo_lookup'],\
                                            offsets['app_offset'],offsets['combo_offset'],write_dir)

##context written elsewhere

