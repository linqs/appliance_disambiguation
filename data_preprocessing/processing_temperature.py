import pandas as pd
from sys import maxsize
import numpy as np
import pickle
import data_formatting as df
from pandas import HDFStore,DataFrame
from process_raw_data import process_dataset
import processing_duration


def make_temperature_df(temperature_series,thresholds):
    to_return = []
    for s_i in range(len(temperature_series)):
        temperature = temperature_series[s_i]
        for t_i in range(len(thresholds)):
            if temperature>=thresholds[t_i][0] and temperature<thresholds[t_i][1]:
                to_return.append(t_i)
    return to_return

def faster_to_datetime(string):
    year = string[:4]
    month=string[5:7]
    day=string[8:10]
    hour=string[11:13]
    minute=string[14:16]
    second=string[17:19]
    return datetime(int(year),int(month),int(day),int(hour),int(minute),int(second))

def get_temperature(weather_file):
    #
    df =  pd.DataFrame.from_csv('../data/original_data/{}.csv'.format(weather_file))
    test=pd.DatetimeIndex(df['localhour'])
    df=df.set_index(test)
    
    return df

def get_austin_temperature(df):
    longitude = -97.699662
    latitude = 30.292432
    
    temp =  df[df['longitude']==longitude ]
    return temp[temp['latitude']==latitude]

def get_austin_temperature_year(year,weather_file):
    temp_df = get_temperature(weather_file)
    temp_df = get_austin_temperature(temp_df)
    years = df_to_year(temp_df)
    return  temp_df[years[year][0]:years[year][-1]]

def df_to_year(df):
    return   df.groupby(lambda x:x.year).groups

def get_thresholds(k,data_series):
    to_return = [[-maxsize]]
    
    i = 0
    for percentile in range(int(100/k),100-int(100/k)+1,int(100/k)):
        threshold = np.percentile(data_series,percentile)
        
        to_return[i].append(threshold)
        to_return.append([threshold])
        i=i+1
    to_return[-1].append(maxsize)
    return to_return

def save_temperature(num_thresholds,year,weather_file):
    df = get_austin_temperature_year(year,weather_file)
    thresholds = get_thresholds(num_thresholds,df['temperature'])
    adjusted_df = make_temperature_df(df['temperature'],thresholds)
    temperature_df = pd.DataFrame({'temperature':adjusted_df},index=df['temperature'].index)
    to_Write = {'temp_df':temperature_df,'thresholds':thresholds}
    ##assumes that all hyper-parameters are stored in the folder threshold settings
    with open('../data/threshold_settings/austin_temperature_df_and_thresholds_{}.pkl'.format(year),'wb') as f:
        pickle.dump(to_Write,f)

def save_temperature_from_threshold_file(threshold_file,year,weather_file):
    df = get_austin_temperature_year(year,weather_file)
    with open('../data/threshold_settings/{}.pkl'.format(threshold_file),'rb') as f:
        thresholds= pickle.load(f)
    
    adjusted_df = make_temperature_df(df['temperature'],thresholds)
    temperature_df = pd.DataFrame({'temperature':adjusted_df},index=df['temperature'].index)
    to_Write = {'temp_df':temperature_df,'thresholds':thresholds}
    ##assumes that all hyper-parameters are stored in the folder threshold settings
    with open('../data/threshold_settings/austin_temperature_df_and_thresholds_{}.pkl'.format(year),'wb') as f:
        pickle.dump(to_Write,f)

if __name__=='__main__':
    save_temperature(3,2015,'austin_weather')
