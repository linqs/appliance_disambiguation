This project includes the code needed to reproduce the results in the paper: 
"Disambiguating Energy Disaggregation: A Collective Probabilistic Approach". If using this code please cite the paper using the following bibtex: 

```tex
@InProceedings{tomkins:ijcai17,
author = {Tomkins, Sabina and Pujara, Jay and Getoor, Lise},
title = {Disambiguating Energy Disaggregation: A Collective Probabilistic Approach},
booktitle = {International Joint Conference on Artificial Intelligence},
year = {2017}}
```


To run there is a basic workflow: 

0. Dependencies. There are a number of python dependencies for preprocessing. Getting sklearn should take care of all of them. The preprocessing scripts are all written in python 3. I recommend using anaconda as it makes it so simple to activate a python 3 environment when running this code. 
1. Get the data
* You will need permissions for both of the datasets used in the paper. To learn more: [REDD](http://redd.csail.mit.edu/), [Dataport](https://dataport.cloud/).
* There are still a few pre-preprocessing steps to do with the data. More details are in [the data collection directory](/data_collection)
2.  Next you will need to convert the data to a format which the probabilistic framework can use. To do so follow the steps in [the data processing directory](/data_preprocessing)
3.  After this you are ready to disaggregate. To do so navigate to [disambiguate directory](/probabilistic_framework/disambiguate). 
4.  When you have successfully disaggregated some data you can process the results in [the evaluation directory](/evaluation). 
5.  Now you can build on the code! The framework is very extensible. To see how you can encode relations look at the files in [the disambiguate package](/probabilistic_framework/disambiguate/src/main/java/disambiguate). If you have any questions contact me at: Sabina email address on paper. 
